#include "ScanPointAdaptor.hpp"

#include <BufferUtils/AlignmentAdaptor.hpp>
#include <BufferUtils/BufferRangeAdaptorExt.hpp>
#include <catch2/catch.hpp>

#include <Eigen/Eigen>
#include <vector>

TEST_CASE("get_coordinate_offset", "[get_coordinate_offset]") {
    GIVEN("a scan point adaptor type") {
        using adaptor = ScanPointAdaptor<                                                  //
            CoordinateOffset<Coordinate::X, 0 * sizeof(double), AlignmentAdaptor<double>>, //
            CoordinateOffset<Coordinate::Y, 1 * sizeof(double), AlignmentAdaptor<double>>, //
            CoordinateOffset<Coordinate::Z, 2 * sizeof(double), AlignmentAdaptor<double>>  //
            >;
        std::vector<double> buf{0.123, 0.234, 0.345};
        adaptor a{reinterpret_cast<std::byte*>(buf.data())};
        WHEN("we retrieve the offset of the X component") {
            constexpr double offsetX = get_coordinate_offset_v<Coordinate::X, adaptor>;
            THEN("the offset matches the one given in the type") {
                REQUIRE(offsetX == 0 * sizeof(double));
            }
        }
        WHEN("we retrieve the offset of the Y component") {
            constexpr double offsetY = get_coordinate_offset_v<Coordinate::Y, adaptor>;
            THEN("the offset matches the one given in the type") {
                REQUIRE(offsetY == 1 * sizeof(double));
            }
        }
        WHEN("we retrieve the offset of the Z component") {
            constexpr double offsetZ = get_coordinate_offset_v<Coordinate::Z, adaptor>;
            THEN("the offset matches the one given in the type") {
                REQUIRE(offsetZ == 2 * sizeof(double));
            }
        }

        WHEN("we retrieve the value of the X component") {
            const double valueX = a.x();
            THEN("the offset matches the one given in the type") {
                REQUIRE(valueX == 0.123);
            }
        }
        WHEN("we change the value of the X component") {
            a.x() = 4.56;
            THEN("retrieving it produces the new value") {
                REQUIRE(a.x() == 4.56);
                REQUIRE(buf[0] == a.x());
            }
        }

        WHEN("we retrieve the value of the Y component") {
            const double valueY = a.y();
            THEN("the offset matches the one given in the type") {
                REQUIRE(valueY == 0.234);
            }
        }
        WHEN("we change the value of the X component") {
            a.y() = 5.67;
            THEN("retrieving it produces the new value") {
                REQUIRE(a.y() == 5.67);
                REQUIRE(buf[1] == a.y());
            }
        }

        WHEN("we retrieve the value of the Z component") {
            const double valueZ = a.z();
            THEN("the offset matches the one given in the type") {
                REQUIRE(valueZ == 0.345);
            }
        }
        WHEN("we change the value of the Z component") {
            a.z() = 6.78;
            THEN("retrieving it produces the new value") {
                REQUIRE(a.z() == 6.78);
                REQUIRE(buf[2] == a.z());
            }
        }
    }
}

TEST_CASE("2d access to 3d points", "[BufferRangeAdaptorExt]") {
    struct ScanPt3d {
        double x, y, z;
    };

    using PointAdaptor = ScanPointAdaptor<                                                //
        CoordinateOffset<Coordinate::X, offsetof(ScanPt3d, x), AlignmentAdaptor<double>>, //
        CoordinateOffset<Coordinate::Y, offsetof(ScanPt3d, y), AlignmentAdaptor<double>>  //
        >;

    std::vector<ScanPt3d> buf{
        ScanPt3d{1.1, 1.2, 1.3}, //
        ScanPt3d{2.1, 2.2, 2.3}, //
        ScanPt3d{3.1, 3.2, 3.3}  //
    };

    BufferRangeAdaptorExt<PointAdaptor, sizeof(ScanPt3d)> adaptedBuffer{
        reinterpret_cast<std::byte*>(buf.data()), reinterpret_cast<std::byte*>(buf.data() + buf.size())};

    std::size_t i{0ul};
    for (auto pt : adaptedBuffer) {
        const double x = pt.x();
        const double xd = x;
        const double y = pt.y();

        REQUIRE(x == buf[i].x);
        const auto newx = x + 1.0;
        pt.x() = newx;
        const double newxd = newx;
        REQUIRE(buf[i].x == x + 1.0);

        REQUIRE(y == buf[i].y);
        pt.y() = y + 2.0;
        REQUIRE(buf[i].y == y + 2.0);

        ++i;
    }
}
