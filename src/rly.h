#pragma once
#include <array>
#include <iterator>
#include <vector>
#if __has_include(<concepts>)
#    include <concepts>
#endif
#if __has_include(<ranges>)
#    include <ranges>
#endif

namespace rly {

struct ScanPoint : std::tuple<double, double, double> {
    using std::tuple<double, double, double>::tuple;

    constexpr double& x() {
        return std::get<0>(*this);
    }
    constexpr double& y() {
        return std::get<1>(*this);
    }
    constexpr double& z() {
        return std::get<2>(*this);
    }
};

class ScanPointIterator {
public:
    using iterator_concept = std::input_iterator_tag;
    using iterator_category = std::random_access_iterator_tag;
    using value_type = ScanPoint;
    using difference_type = long;
    using pointer = ScanPoint*;
    using reference = value_type&;

private:
    ScanPoint m_pt;

public:
    ScanPointIterator() {}
    ScanPointIterator(const ScanPointIterator& other) = default;
    ScanPointIterator(ScanPointIterator&& other) = default;
    ScanPointIterator& operator=(const ScanPointIterator& other) = default;
    ScanPointIterator& operator=(ScanPointIterator&& other) = default;

    friend bool operator==(const ScanPointIterator& a, const ScanPointIterator& b) {
        return true;
    };

    friend bool operator!=(const ScanPointIterator& a, const ScanPointIterator& b) {
        return !(a == b);
    };

    auto operator*() const -> ScanPoint const& {
        return m_pt;
    }

    auto operator*() -> ScanPoint const& {
        return m_pt;
    }

    auto operator->() const -> ScanPoint const* {
        return &m_pt;
    }

    auto operator->() -> ScanPoint* {
        return &m_pt;
    }

    auto operator++() -> ScanPointIterator& {
        return *this;
    }

    auto operator++(int) -> ScanPointIterator {
        auto copy{*this};
        return copy;
    }

    friend auto operator-(const ScanPointIterator& a, const ScanPointIterator& b) {
        return 0;
    }
};

class ScanPointsRange
#if __has_include(<ranges>)
    : public std::ranges::view_interface<ScanPointsRange>
#endif
{
public:
    auto begin() const -> ScanPointIterator {
        return {};
    }

    auto end() const -> ScanPointIterator {
        return {};
    }
};

class PointCloud {
public:
    auto range() -> ScanPointsRange {
        return {};
    }
};

struct ColorRGB {
    std::uint8_t r, g, b;
};

#if __has_include(<concepts>)
template<typename T>
concept IsCartesianPoint = requires(T pt) {
    {pt.x()};
    {pt.y()};
    {pt.z()};
};

template<typename T>
concept IsSphericalScanPoint = requires(T pt) {
    {pt.rho()};
    {pt.theta()};
    {pt.phi()};
};

template<typename T>
concept IsScanPoint = IsCartesianPoint<T> || IsCartesianPoint<T>;

template<typename T>
concept IsColorizedScanPoint = IsCartesianPoint<T>&& requires(T&& pt) {
    { pt.color() }
    ->std::same_as<ColorRGB>;
};
#endif

} // namespace rly