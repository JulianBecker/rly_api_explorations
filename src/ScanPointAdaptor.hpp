#pragma once
#include <cstddef>

enum class Coordinate { X, Y, Z };

template<Coordinate C, std::size_t Offset, typename ComponentAccessAdaptor>
struct CoordinateOffset {
    constexpr static auto coordinate = C;
    constexpr static auto offset = Offset;
    using type = CoordinateOffset;
    using access_adaptor_type = ComponentAccessAdaptor;
};

//
template<Coordinate C, typename Config>
struct get_coordinate_offset;

template<Coordinate C, template<typename...> class ConfigT, typename Component, typename... Rest>
struct get_coordinate_offset<C, ConfigT<Component, Rest...>> : get_coordinate_offset<C, ConfigT<Rest...>> {};

template<Coordinate C, template<typename...> class ConfigT, std::size_t Offset, typename ComponentAccessAdaptor,
         typename... Rest>
struct get_coordinate_offset<C, ConfigT<CoordinateOffset<C, Offset, ComponentAccessAdaptor>, Rest...>> {
    static constexpr auto value = Offset;
};

template<Coordinate C, typename Config>
static constexpr std::size_t get_coordinate_offset_v = get_coordinate_offset<C, Config>::value;

//
template<Coordinate C, typename Config>
struct get_coordinate_adaptor {
    using type = get_coordinate_adaptor;
};

template<Coordinate C, template<typename...> class ConfigT, typename Component, typename... Rest>
struct get_coordinate_adaptor<C, ConfigT<Component, Rest...>> : get_coordinate_adaptor<C, ConfigT<Rest...>> {};

template<Coordinate C, template<typename...> class ConfigT, std::size_t Offset, typename ComponentAccessAdaptor,
         typename... Rest>
struct get_coordinate_adaptor<C, ConfigT<CoordinateOffset<C, Offset, ComponentAccessAdaptor>, Rest...>> {
    using type = ComponentAccessAdaptor;
};

template<Coordinate C, typename Config>
using get_coordinate_adaptor_t = typename get_coordinate_adaptor<C, Config>::type;

//
template<Coordinate C, typename Config>
struct has_component : std::false_type {};

template<Coordinate C, template<typename...> class ConfigT, typename Component, typename... Rest>
struct has_component<C, ConfigT<Component, Rest...>> : has_component<C, ConfigT<Rest...>> {};

template<Coordinate C, template<typename...> class ConfigT, std::size_t Offset, typename ComponentAccessAdaptor,
         typename... Rest>
struct has_component<C, ConfigT<CoordinateOffset<C, Offset, ComponentAccessAdaptor>, Rest...>> : std::true_type {};

template<Coordinate C, typename Config>
static constexpr bool has_component_v = has_component<C, Config>::value;

struct EmptyStruct {};

template<Coordinate C, typename Derived>
struct ComponentAccess {
    static constexpr Coordinate coordinate = C;
    using access_adaptor_type = get_coordinate_adaptor_t<C, Derived>;
    static constexpr auto offset = get_coordinate_offset_v<C, Derived>;

    auto data() const {
        return static_cast<Derived const*>(this)->dataBase() + offset;
    }
};

template<typename Derived>
struct ScanPointWithX : ComponentAccess<Coordinate::X, Derived> {
    using access_adaptor_type = typename ComponentAccess<Coordinate::X, Derived>::access_adaptor_type;
    using value_type = typename access_adaptor_type::value_type;

    auto x() const {
        return access_adaptor_type{this->data()};
    }

    // void set_x(value_type x) {
    //     access_adaptor_type{this->data()} = x;
    // }
};

template<typename Derived>
struct ScanPointWithY : ComponentAccess<Coordinate::Y, Derived> {
    using access_adaptor_type = typename ComponentAccess<Coordinate::Y, Derived>::access_adaptor_type;
    using value_type = typename access_adaptor_type::value_type;

    auto y() const {
        return access_adaptor_type{this->data()};
    }

    // void set_y(value_type y) {
    //     access_adaptor_type{this->data()} = y;
    // }
};

template<typename Derived>
struct ScanPointWithZ : ComponentAccess<Coordinate::Z, Derived> {
    using access_adaptor_type = typename ComponentAccess<Coordinate::Z, Derived>::access_adaptor_type;
    using value_type = typename access_adaptor_type::value_type;

    auto z() const {
        return access_adaptor_type{this->data()};
    }

    // void set_z(value_type z) {
    //     access_adaptor_type{this->data()} = z;
    // }
};

template<typename Derived>
struct MixinX : std::conditional_t<has_component_v<Coordinate::X, Derived>, ScanPointWithX<Derived>, EmptyStruct> {};

template<typename Derived>
struct MixinY : std::conditional_t<has_component_v<Coordinate::Y, Derived>, ScanPointWithY<Derived>, EmptyStruct> {};

template<typename Derived>
struct MixinZ : std::conditional_t<has_component_v<Coordinate::Z, Derived>, ScanPointWithZ<Derived>, EmptyStruct> {};

template<typename... Components>
struct ScanPointAdaptor
    : MixinX<ScanPointAdaptor<Components...>>
    , MixinY<ScanPointAdaptor<Components...>>
    , MixinZ<ScanPointAdaptor<Components...>> {
private:
    std::byte* m_ptr;

public:
    ScanPointAdaptor(std::byte* ptr)
        : m_ptr{ptr} {}

    auto dataBase() const {
        return m_ptr;
    }
};
