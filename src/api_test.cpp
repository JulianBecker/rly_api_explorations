#include "rly.h"

#include <catch2/catch.hpp>

#include <algorithm>
#if __has_include(<ranges>)
#    include <ranges>
#endif
#if __has_include(<concepts>)
#    include <concepts>
#endif

using namespace rly;

TEST_CASE("point-cloud interface basics", "[rly-api]") {
    GIVEN("a point-cloud") {
        auto pc = PointCloud{};
        THEN("there should be a range() member to access the range of scan points") {
            (void) pc.range();
        }
        THEN("we should be able to iterate over the points with a range-based for loop") {
            for (const auto point : pc.range()) {}
        }

        THEN("we should be able to copy scan points into an std::vector using std::copy") {
            std::vector<ScanPoint> copy;
            auto range{pc.range()};
            std::copy(range.begin(), range.end(), std::back_inserter(copy));
#if __has_include(<ranges>)
            std::ranges::copy(range, std::back_inserter(copy));
            static_assert(std::ranges::input_range<ScanPointsRange>);
#endif
        }
    }
}

auto center_of_mass(ScanPointsRange&& r) -> ScanPoint {
    ScanPoint center{0., 0., 0.};
    int n = 0;
    for (auto pt : r) {
        ++n;
        center.x() += (pt.x() - center.x()) / n;
        center.y() += (pt.y() - center.y()) / n;
        center.z() += (pt.z() - center.z()) / n;
    }
    return center;
}

TEST_CASE("Calculating the center of mass", "[rly-api]") {
    GIVEN("a point-cloud") {

        auto pc = PointCloud{};
        THEN("we can calculate the center of mass easily") {
            (void) pc.range();
            auto center = center_of_mass(pc.range());
        }
    }
}