#include <FileIO/ConcatenatedFiles.hpp>
#include <catch2/catch.hpp>

TEST_CASE("ConcatenatedFiles", "[ConcatenatedFiles]") {
    GIVEN("two files containing some data") {
        const auto testData1 = std::string{"some data"};
        const auto tempFilePath1{std::filesystem::temp_directory_path() / "tmpfile1"};
        {
            std::ofstream dummyFile{tempFilePath1.string()};
            dummyFile << testData1;
            dummyFile.close();
        }
        const auto testData2 = std::string{"%#$b:+:(+:!‚"};
        const auto tempFilePath2{std::filesystem::temp_directory_path() / "tmpfile2"};
        {
            std::ofstream dummyFile{tempFilePath2.string()};
            dummyFile << testData2;
            dummyFile.close();
        }
        WHEN("we create a concatenated view of the files") {
            auto files = std::vector{tempFilePath1, tempFilePath2};
            auto concatenated = ConcatenatedFilesRange(files.begin(), files.end());
            THEN("the concatenated view matches the concatenation of the data that was written to the files that were "
                 "concatenated") {
                std::vector<std::byte> data;
                std::copy(concatenated.begin(), concatenated.end(), std::back_inserter(data));
                std::string dataStr(reinterpret_cast<char const*>(data.data()),
                                    reinterpret_cast<char const*>(data.data() + data.size()));

                REQUIRE(dataStr == testData1 + testData2);
            }
        }
    }
}