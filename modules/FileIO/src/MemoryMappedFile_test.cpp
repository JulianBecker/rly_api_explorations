#include <FileIO/MemoryMappedFile.hpp>
#include <catch2/catch.hpp>

TEST_CASE("MemoryMappedFile", "[MemoryMappedFile]") {
    GIVEN("A file containing some data") {
        const auto testData = std::string{"some data"};
        const auto tempFilePath{std::filesystem::temp_directory_path() / "tmpfile"};
        std::ofstream dummyFile{tempFilePath.string()};
        dummyFile << testData;
        dummyFile.close();
        WHEN("we map the file into memory using MemoryMappedFile") {
            MemoryMappedFile map(tempFilePath);
            THEN("the contents match the data written to the file") {
                const auto readData = std::string(reinterpret_cast<char const*>(map.data()), map.size());

                REQUIRE(readData == testData);
            }
        }
    }
}