#include <FileIO/MemoryMappedFile.hpp>

#include <cstdint>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <iterator>
#include <optional>
#include <string>
#include <system_error>

#ifdef _WIN32
#    ifndef WIN32_LEAN_AND_MEAN
#        define WIN32_LEAN_AND_MEAN
#    endif
#    include <windows.h>
#else
#    include <fcntl.h>
#    include <sys/mman.h>
#    include <sys/stat.h>
#    include <unistd.h>
#endif

#ifdef _WIN32

class MemoryMappedFile::MemoryMappedFileImpl final : public MemoryMappedFile::IImpl {
    using handle_type = HANDLE;
    std::optional<handle_type> m_fileHandle{std::nullopt};

    struct MMapInfo {
        std::byte* data;
        std::int64_t size;
        handle_type mappingHandle;
    };

    std::optional<MMapInfo> m_mapInfo{std::nullopt};

    inline MMapInfo mapMemory(const HANDLE fileHandle, const int64_t size) {
        constexpr auto sizeLowWord = [](std::int64_t n) noexcept { return n & 0xffffffff; };
        constexpr auto sizeHighWord = [](std::int64_t n) noexcept { return (n >> 32) & 0xffffffff; };

        const auto mappingHandle =
            ::CreateFileMapping(fileHandle, 0, PAGE_READONLY, sizeHighWord(size), sizeLowWord(size), 0);
        if (mappingHandle == INVALID_HANDLE_VALUE) {
            throw querySystemError();
        }
        const auto base = static_cast<std::byte*>(::MapViewOfFile(mappingHandle, FILE_MAP_READ, 0, 0, size));
        if (base == nullptr) {
            const auto error{querySystemError()};
            ::CloseHandle(mappingHandle);
            throw error;
        }

        return {.data = base, .size = size, .mappingHandle = mappingHandle};
    }

    static inline auto querySystemError() noexcept -> std::system_error {
        std::error_code error;
        error.assign(GetLastError(), std::system_category());
        return {error};
    }

    void mapHandle(const handle_type handle) {
        const auto fileSize = determineFileSize(handle);
        const auto mapInfo = mapMemory(handle, fileSize);
        unmap();
        m_mapInfo = mapInfo;
    }

    static handle_type openFile(const std::filesystem::path& path) {
        const auto handle = ::CreateFileA(path.string().c_str(), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, 0,
                                          OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
        if (handle == INVALID_HANDLE_VALUE) {
            throw querySystemError();
        }
        return handle;
    }

    static inline auto determineFileSize(handle_type handle) -> std::size_t {
        LARGE_INTEGER fileSize{};
        if (::GetFileSizeEx(handle, &fileSize) == 0) {
            throw querySystemError();
        }
        return static_cast<std::size_t>(fileSize.QuadPart);
    }

public:
    MemoryMappedFileImpl& operator=(MemoryMappedFileImpl&& other) {
        m_mapInfo = std::exchange(other.m_mapInfo, std::nullopt);
        return *this;
    }

    handle_type fileHandle() const noexcept {
        return m_fileHandle.value();
    }

public:
    void map(const std::filesystem::path& path) override {
        const auto handle = openFile(path);
        mapHandle(handle);
    }

    void unmap() override {
        if (!m_mapInfo.has_value()) {
            return;
        }
        if (m_mapInfo->mappingHandle) {
            ::UnmapViewOfFile(m_mapInfo->data);
            ::CloseHandle(m_mapInfo->mappingHandle);
        }
        m_mapInfo = std::nullopt;

        if (m_fileHandle) {
            ::CloseHandle(*m_fileHandle);
        }
        m_fileHandle = std::nullopt;
    }

    auto data() const noexcept -> std::byte const* override {
        return m_mapInfo.value().data;
    }

    auto size() const noexcept -> std::size_t override {
        return m_mapInfo.value().size;
    }
};

#else

class MemoryMappedFile::MemoryMappedFileImpl final : public MemoryMappedFile::IImpl {
    using handle_type = int;
    std::optional<handle_type> m_fileHandle{std::nullopt};

    struct MMapInfo {
        std::byte* data;
        std::int64_t size;
    };

    std::optional<MMapInfo> m_mapInfo{std::nullopt};

    inline MMapInfo mapMemory(const handle_type fileHandle, const int64_t size) {
        constexpr auto sizeLowWord = [](std::int64_t n) noexcept { return n & 0xffffffff; };
        constexpr auto sizeHighWord = [](std::int64_t n) noexcept { return (n >> 32) & 0xffffffff; };

        const auto base = static_cast<std::byte*>(::mmap(0, size, PROT_READ, MAP_SHARED, fileHandle, 0));
        if (base == MAP_FAILED) {
            throw querySystemError();
        }

        return {.data = base, .size = size};
    }

    static inline auto querySystemError() noexcept -> std::system_error {
        std::error_code error;
        error.assign(errno, std::system_category());
        return {error};
    }

    void mapHandle(const handle_type handle) {
        const auto fileSize = determineFileSize(handle);
        const auto mapInfo = mapMemory(handle, fileSize);
        unmap();
        m_mapInfo = mapInfo;
    }

    static handle_type openFile(const std::filesystem::path& path) {
        const auto handle = ::open(path.c_str(), O_RDONLY);
        if (handle == -1) {
            throw querySystemError();
        }

        return handle;
    }

    static inline auto determineFileSize(handle_type handle) -> std::size_t {
        struct stat sbuf;
        if (::fstat(handle, &sbuf) == -1) {
            throw querySystemError();
        }
        return sbuf.st_size;
    }

    handle_type fileHandle() const noexcept {
        return m_fileHandle.value();
    }

public:
    void map(const std::filesystem::path& path) override {
        const auto handle = openFile(path);
        mapHandle(handle);
    }

    auto size() const noexcept -> std::size_t override {
        return m_mapInfo.value().size;
    }

    auto data() const noexcept -> std::byte const* override {
        return m_mapInfo.value().data;
    }

    void unmap() override {
        if (!m_mapInfo.has_value())
            return;

        if (m_mapInfo->data) {
            ::munmap(m_mapInfo->data, m_mapInfo->size);
        }
        m_mapInfo = std::nullopt;

        if (m_fileHandle) {
            ::close(*m_fileHandle);
        }
        m_fileHandle = std::nullopt;
    }
};

#endif

MemoryMappedFile::MemoryMappedFile(const std::filesystem::path& path)
    : m_impl{std::make_unique<MemoryMappedFileImpl>()} {
    m_impl->map(path);
}

MemoryMappedFile::~MemoryMappedFile() {
    if (m_impl)
        m_impl->unmap();
}
