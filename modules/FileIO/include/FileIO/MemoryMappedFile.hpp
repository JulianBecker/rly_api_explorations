#pragma once
#include <cstdint>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <iterator>
#include <optional>
#include <string>
#include <system_error>

struct MemoryMappedFile {
    class IImpl {
    public:
        virtual ~IImpl() = default;
        virtual void map(std::filesystem::path const&) = 0;
        virtual void unmap() = 0;
        virtual auto size() const noexcept -> std::size_t = 0;
        virtual auto data() const noexcept -> std::byte const* = 0;
    };

    class MemoryMappedFileImpl;

    std::unique_ptr<IImpl> m_impl;

public:
    using value_type = std::byte;
    using const_iterator = const value_type*;
    using iterator = const_iterator;

    MemoryMappedFile(const std::filesystem::path& path);

    MemoryMappedFile(const MemoryMappedFile&) = delete;
    MemoryMappedFile& operator=(const MemoryMappedFile&) = delete;
    MemoryMappedFile(MemoryMappedFile&& other) = default;
    MemoryMappedFile& operator=(MemoryMappedFile&& other) = default;

    ~MemoryMappedFile();

    auto size() const noexcept -> std::size_t {
        return m_impl->size();
    }

    auto data() const noexcept -> std::byte const* {
        return m_impl->data();
    }

    auto begin() const noexcept -> const_iterator {
        return m_impl->data();
    }

    auto end() const noexcept -> const_iterator {
        return m_impl->data() + m_impl->size();
    }

    auto operator[](const std::size_t i) const noexcept -> std::byte const& {
        return m_impl->data()[i];
    }
};
