#pragma once
#include <FileIO/MemoryMappedFile.hpp>

#include <memory>
#include <vector>

template<typename Iterator, typename End>
class ConcatenatedFilesIterator {
public:
    using value_type = std::byte const;
    using iterator_type = ConcatenatedFilesIterator;
    using iterator_category = std::forward_iterator_tag;
    using difference_type = long;
    using pointer = MemoryMappedFile::iterator;
    using reference = value_type&;

    Iterator m_fileIt;
    End m_end;
    std::shared_ptr<MemoryMappedFile> m_file;
    MemoryMappedFile::iterator m_p{};

    ConcatenatedFilesIterator() = default;
    ConcatenatedFilesIterator(ConcatenatedFilesIterator const&) = default;
    ConcatenatedFilesIterator& operator=(ConcatenatedFilesIterator const&) = default;
    ConcatenatedFilesIterator(ConcatenatedFilesIterator&&) = default;
    ConcatenatedFilesIterator& operator=(ConcatenatedFilesIterator&&) = default;

    template<typename I, typename E>
    ConcatenatedFilesIterator(I&& fileIt, E&& end)
        : m_fileIt{std::forward<I>(fileIt)}
        , m_end{std::forward<E>(end)}
        , m_file{ m_fileIt == m_end ? nullptr : std::make_shared<MemoryMappedFile>(*m_fileIt)}
        , m_p{m_file ? m_file->begin() : nullptr} {}

    friend bool operator==(const iterator_type& a, const iterator_type& b) {
        return (a.m_fileIt == a.m_end && b.m_fileIt == b.m_end) || (a.m_fileIt == b.m_fileIt && a.m_p == b.m_p);
    }

    friend bool operator!=(const iterator_type& a, const iterator_type& b) {
        return !(a == b);
    }

    auto operator*() const -> reference {
        return *m_p;
    }

    auto operator->() const -> pointer {
        return m_p;
    }

    auto operator++() -> iterator_type& {
        ++m_p;
        if (m_p == m_file->end()) {
            ++m_fileIt;
            if (m_fileIt != m_end) {
                m_file = std::make_shared<MemoryMappedFile>(*m_fileIt);
                m_p = m_file->begin();
            } else {
                m_file = nullptr;
                m_p = MemoryMappedFile::iterator{};
            }
        }

        return *this;
    }

    auto operator++(int) -> iterator_type {
        auto copy(*this);
        ++(*this);
        return copy;
    }
};

template<typename Iterator, typename End>
class ConcatenatedFilesRange {
    using value_type = std::byte;
    using const_iterator_type = ConcatenatedFilesIterator<Iterator, End>;
    using iterator_type = const_iterator_type;

    Iterator m_begin;
    End m_end;

public:
    template<typename B, typename E>
    ConcatenatedFilesRange(B&& first, E&& end)
        : m_begin{std::forward<B>(first)}
        , m_end{std::forward<E>(end)} {}

    auto begin() -> ConcatenatedFilesIterator<Iterator, End> {
        return {m_begin, m_end};
    }

    auto end() -> ConcatenatedFilesIterator<Iterator, End> {
        return {m_end, m_end};
    }
};

template<typename B, typename E>
ConcatenatedFilesRange(B&& first, E&& end) -> ConcatenatedFilesRange<B, E>;
