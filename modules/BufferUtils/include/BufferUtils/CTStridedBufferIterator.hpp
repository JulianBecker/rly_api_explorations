#pragma once
#include <cstring>
#include <iterator>

/// A variant of StridedBufferIterator, where the stride is known at compile-time
template<std::size_t Stride>
class CTStridedBufferIterator {
    std::byte* m_pos;
    std::byte* m_end;

public:
    using iterator_type = CTStridedBufferIterator;
    using iterator_concept = std::input_iterator_tag;
    using iterator_category = std::random_access_iterator_tag;
    using value_type = std::byte*;
    using difference_type = long;
    using pointer = value_type*;
    using reference = value_type&;

    CTStridedBufferIterator()
        : m_pos{nullptr}
        , m_end{nullptr} {}

    CTStridedBufferIterator(std::byte* pos, std::byte* end)
        : m_pos{pos}
        , m_end{end} {}

    CTStridedBufferIterator(const CTStridedBufferIterator& other) = default;
    CTStridedBufferIterator(CTStridedBufferIterator&& other) = default;
    CTStridedBufferIterator& operator=(const CTStridedBufferIterator& other) = default;
    CTStridedBufferIterator& operator=(CTStridedBufferIterator&& other) = default;

    friend bool operator==(const CTStridedBufferIterator& a, const CTStridedBufferIterator& b) {
        return (a.m_pos == b.m_pos) || (a.m_end <= a.m_pos && b.m_end <= b.m_pos);
    }

    friend bool operator!=(const CTStridedBufferIterator& a, const CTStridedBufferIterator& b) {
        return !(a == b);
    }

    auto operator*() const -> value_type const& {
        return m_pos;
    }

    auto operator*() -> value_type const& {
        return m_pos;
    }

    auto operator->() const -> value_type const* {
        return &m_pos;
    }

    auto operator->() -> value_type* {
        return &m_pos;
    }

    auto operator++() -> iterator_type& {
        m_pos += Stride;
        return *this;
    }

    auto operator++(int) -> iterator_type {
        auto copy{*this};
        ++(*this);
        return copy;
    }

    friend auto operator-(const iterator_type& a, const iterator_type& b) {
        return (a.m_pos - b.m_pos) / Stride;
    }
};
