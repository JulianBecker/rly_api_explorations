#pragma once
#include <cstddef>
#include <cstring>
#if __has_include(<ranges>)
#    include <ranges>
#endif
class BufferRange
#if __has_include(<ranges>)
    : public std::ranges::view_interface<BufferRange>
#endif
{
    std::byte* m_begin;
    std::byte* m_end;

public:
    BufferRange(std::byte* begin, std::byte* end)
        : m_begin{begin}
        , m_end{end} {}

    BufferRange(std::byte* begin, std::size_t size)
        : m_begin{begin}
        , m_end{begin + size} {}

    auto begin() const -> std::byte* {
        return m_begin;
    }

    auto end() const -> std::byte* {
        return m_end;
    }
};
