#pragma once
#include "AdaptedBufferRangeIterator.hpp"
#include "CTStridedBufferRange.hpp"
#if __has_include(<ranges>)
#    include <ranges>
#endif

template<typename Adaptor, std::size_t Stride>
class BufferRangeAdaptorExt
#if __has_include(<ranges>)
    : public std::ranges::view_interface<BufferRangeAdaptorExt<Adaptor, Stride>>
#endif
{
    CTStridedBufferRange<Stride> m_range;

public:
    BufferRangeAdaptorExt(std::byte* begin, std::byte* end)
        : m_range{begin, end} {}

    BufferRangeAdaptorExt(std::byte* begin, std::size_t size)
        : m_range{begin, size * Stride} {}

    auto begin() const -> AdaptedBufferRangeExtIterator<Adaptor, Stride> {
        return {m_range.begin()};
    }

    auto end() const -> AdaptedBufferRangeExtIterator<Adaptor, Stride> {
        return {};
    }
};