#pragma once
#include "CTStridedBufferIterator.hpp"

#include <iterator>
#include <type_traits>

template<typename T, std::size_t Stride>
class AdaptedBufferRangeIterator {
    CTStridedBufferIterator<Stride> m_it;
    std::aligned_storage_t<sizeof(T), alignof(T)> m_storage;

public:
    using iterator_type = AdaptedBufferRangeIterator;
    using iterator_concept = std::input_iterator_tag;
    using iterator_category = std::random_access_iterator_tag;
    using value_type = T;
    using difference_type = long;
    using pointer = value_type*;
    using reference = value_type&;

    AdaptedBufferRangeIterator() = default;

    AdaptedBufferRangeIterator(CTStridedBufferIterator<Stride> it)
        : m_it{std::move(it)} {
        std::memcpy((void*) &m_storage, (const void*) *m_it, sizeof(T));
    }

    AdaptedBufferRangeIterator(const AdaptedBufferRangeIterator& other) = default;
    AdaptedBufferRangeIterator(AdaptedBufferRangeIterator&& other) = default;
    AdaptedBufferRangeIterator& operator=(const AdaptedBufferRangeIterator& other) = default;
    AdaptedBufferRangeIterator& operator=(AdaptedBufferRangeIterator&& other) = default;

    friend bool operator==(const AdaptedBufferRangeIterator& a, const AdaptedBufferRangeIterator& b) {
        return a.m_it == b.m_it;
    }

    friend bool operator!=(const AdaptedBufferRangeIterator& a, const AdaptedBufferRangeIterator& b) {
        return !(a == b);
    }

    auto operator*() const -> value_type const& {
        return *reinterpret_cast<T const*>(&m_storage);
    }

    auto operator*() -> value_type const& {
        return *reinterpret_cast<T*>(&m_storage);
    }

    auto operator->() const -> value_type const* {
        return reinterpret_cast<T const*>(&m_storage);
    }

    auto operator->() -> value_type* {
        return reinterpret_cast<T*>(&m_storage);
    }

    auto operator++() -> iterator_type& {
        ++m_it;
        std::memcpy((void*) &m_storage, (const void*) *m_it, sizeof(T));
        return *this;
    }

    auto operator++(int) -> iterator_type {
        auto copy{*this};
        ++(*this);
        return copy;
    }

    friend auto operator-(const iterator_type& a, const iterator_type& b) {
        return a.m_it - b.m_it;
    }
};

template<typename Adaptor, std::size_t Stride>
class AdaptedBufferRangeExtIterator {
    CTStridedBufferIterator<Stride> m_it;
    Adaptor m_adaptor{nullptr};

public:
    using iterator_type = AdaptedBufferRangeExtIterator;
    using iterator_concept = std::input_iterator_tag;
    using iterator_category = std::random_access_iterator_tag;
    using value_type = Adaptor;
    using difference_type = long;
    using pointer = value_type*;
    using reference = value_type&;

    AdaptedBufferRangeExtIterator() = default;

    AdaptedBufferRangeExtIterator(CTStridedBufferIterator<Stride> it)
        : m_it{std::move(it)}
        , m_adaptor{*m_it} {}

    AdaptedBufferRangeExtIterator(const AdaptedBufferRangeExtIterator& other) = default;
    AdaptedBufferRangeExtIterator(AdaptedBufferRangeExtIterator&& other) = default;
    AdaptedBufferRangeExtIterator& operator=(const AdaptedBufferRangeExtIterator& other) = default;
    AdaptedBufferRangeExtIterator& operator=(AdaptedBufferRangeExtIterator&& other) = default;

    friend bool operator==(const AdaptedBufferRangeExtIterator& a, const AdaptedBufferRangeExtIterator& b) {
        return a.m_it == b.m_it;
    }

    friend bool operator!=(const AdaptedBufferRangeExtIterator& a, const AdaptedBufferRangeExtIterator& b) {
        return !(a == b);
    }

    auto operator*() const -> value_type const& {
        return m_adaptor;
    }

    auto operator*() -> value_type const& {
        return m_adaptor;
    }

    auto operator->() const -> value_type const* {
        return &m_adaptor;
    }

    auto operator->() -> value_type* {
        return &m_adaptor;
    }

    auto operator++() -> iterator_type& {
        ++m_it;
        m_adaptor = Adaptor{*m_it};
        return *this;
    }

    auto operator++(int) -> iterator_type {
        auto copy{*this};
        ++(*this);
        return copy;
    }

    friend auto operator-(const iterator_type& a, const iterator_type& b) {
        return (a.m_it - b.m_it);
    }
};
