#pragma once
#include "StridedBufferIterator.hpp"

#include <cstddef>
#include <iterator>
#if __has_include(<ranges>)
#    include <ranges>
#endif

class StridedBufferRange
#if __has_include(<ranges>)
    : public std::ranges::view_interface<StridedBufferRange>
#endif
{
    std::byte* m_begin;
    std::byte* m_end;
    std::size_t m_stride;

public:
    StridedBufferRange(std::byte* begin, std::byte* end, std::size_t stride)
        : m_begin{begin}
        , m_end{end}
        , m_stride{stride} {}

    StridedBufferRange(std::byte* begin, std::size_t size, std::size_t stride)
        : m_begin{begin}
        , m_end{begin + size}
        , m_stride{stride} {}

    auto begin() const -> StridedBufferIterator {
        return {m_begin, m_end, m_stride};
    }

    auto end() const -> StridedBufferIterator {
        return {};
    }
};
