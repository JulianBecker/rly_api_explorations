#pragma once
#include <algorithm>
#include <cstddef>

enum class Alignment : std::size_t { Align8 = 8, Align16 = 16, Align32 = 32, Align64 = 64, Align128 = 128 };
enum class Endian { Little, Big };

#if defined(__BYTE_ORDER) && __BYTE_ORDER == __BIG_ENDIAN || defined(__BIG_ENDIAN__) || defined(__ARMEB__) || \
    defined(__THUMBEB__) || defined(__AARCH64EB__) || defined(_MIBSEB) || defined(__MIBSEB) || defined(__MIBSEB__)
inline constexpr auto NativeByteOrder = Endian::Big;
#elif defined(__BYTE_ORDER) && __BYTE_ORDER == __LITTLE_ENDIAN || defined(__LITTLE_ENDIAN__) || defined(__ARMEL__) || \
    defined(__THUMBEL__) || defined(__AARCH64EL__) || defined(_MIPSEL) || defined(__MIPSEL) || defined(__MIPSEL__) || \
    defined(_WINDOWS)
inline constexpr auto NativeByteOrder = Endian::Little;
#else
#    error "I don't know what architecture this is!"
#endif

class AlignmentAdatorBase {
    std::byte* m_ptr;

public:
    AlignmentAdatorBase(AlignmentAdatorBase const&) = delete;
    AlignmentAdatorBase(AlignmentAdatorBase&& other)
        : m_ptr{std::exchange(other.m_ptr, nullptr)} {}
    AlignmentAdatorBase& operator=(AlignmentAdatorBase const&) = delete;
    AlignmentAdatorBase& operator=(AlignmentAdatorBase&& other) {
        m_ptr = std::exchange(other.m_ptr, nullptr);
        return *this;
    }

    explicit AlignmentAdatorBase(std::byte* ptr)
        : m_ptr{ptr} {}

    auto data() const {
        return m_ptr;
    }
};

template<typename T, Alignment A = static_cast<Alignment>(8ul * alignof(T)), Endian E = NativeByteOrder,
         typename = void>
class AlignmentAdaptor;

template<typename T, Alignment A>
class AlignmentAdaptor<T, A, NativeByteOrder> : public AlignmentAdatorBase {
public:
    using AlignmentAdatorBase::AlignmentAdatorBase;
    using AlignmentAdatorBase::operator=;
    using value_type = T;

    AlignmentAdaptor const& operator=(const T& value) const {
        std::copy_n(reinterpret_cast<std::byte const*>(&value), sizeof(T), this->data());
        return *this;
    }

    auto get() const {
        std::aligned_storage_t<sizeof(T), static_cast<std::size_t>(A) / 8ul> storage;
        auto x = sizeof(storage);
        std::copy_n(this->data(), sizeof(T), reinterpret_cast<std::byte*>(&storage));
        return *reinterpret_cast<T*>(&storage);
    }

    operator T() const {
        return get();
    }
};

template<typename T, Alignment A, Endian E>
class AlignmentAdaptor<T, A, E, std::enable_if_t<E != NativeByteOrder>> : public AlignmentAdatorBase {
public:
    using AlignmentAdatorBase::AlignmentAdatorBase;
    using AlignmentAdatorBase::operator=;
    using value_type = T;

    AlignmentAdaptor& operator=(const T& value) {
        std::copy_n(std::make_reverse_iterator(reinterpret_cast<std::byte const*>(&value) + sizeof(T)), sizeof(T),
                    this->data());
        return *this;
    }

    auto get() const -> T {
        std::aligned_storage_t<sizeof(T), static_cast<std::size_t>(A) / 8ul> storage;
        auto x = sizeof(storage);
        std::copy_n(std::make_reverse_iterator(this->data() + sizeof(T)), sizeof(T),
                    reinterpret_cast<std::byte*>(&storage));
        return *reinterpret_cast<T*>(&storage);
    }
};