#pragma once
#include "AdaptedBufferRangeIterator.hpp"
#include "CTStridedBufferRange.hpp"
#if __has_include(<ranges>)
#    include <ranges>
#endif

template<typename T, std::size_t Stride = sizeof(T)>
class BufferRangeAdaptor
#if __has_include(<ranges>)
    : public std::ranges::view_interface<BufferRangeAdaptor<T>>
#endif
{
    CTStridedBufferRange<Stride> m_range;

public:
    BufferRangeAdaptor(std::byte* begin, std::byte* end)
        : m_range{begin, end} {}

    BufferRangeAdaptor(std::byte* begin, std::size_t size)
        : m_range{begin, size * Stride} {}

    auto begin() const -> AdaptedBufferRangeIterator<T, Stride> {
        return {m_range.begin()};
    }

    auto end() const -> AdaptedBufferRangeIterator<T, Stride> {
        return {};
    }
};
