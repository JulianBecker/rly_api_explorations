#pragma once
#include "CTStridedBufferIterator.hpp"
#if __has_include(<ranges>)
#    include <ranges>
#endif

template<std::size_t Stride>
class CTStridedBufferRange
#if __has_include(<ranges>)
    : public std::ranges::view_interface<CTStridedBufferRange<Stride>>
#endif
{
    std::byte* m_begin;
    std::byte* m_end;

public:
    CTStridedBufferRange(std::byte* begin, std::byte* end)
        : m_begin{begin}
        , m_end{end} {}

    CTStridedBufferRange(std::byte* begin, std::size_t size)
        : m_begin{begin}
        , m_end{begin + size} {}

    auto begin() const -> CTStridedBufferIterator<Stride> {
        return {m_begin, m_end};
    }

    auto end() const -> CTStridedBufferIterator<Stride> {
        return {};
    }
};
