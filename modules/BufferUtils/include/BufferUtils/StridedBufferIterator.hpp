#pragma once
#include <cstddef>
#include <iterator>

class StridedBufferIterator {
    std::byte* m_pos;
    std::byte* m_end;
    std::size_t m_stride;

public:
    using iterator_type = StridedBufferIterator;
    using iterator_concept = std::input_iterator_tag;
    using iterator_category = std::random_access_iterator_tag;
    using value_type = std::byte*;
    using difference_type = long;
    using pointer = value_type*;
    using reference = value_type&;

    StridedBufferIterator()
        : m_pos{nullptr}
        , m_end{nullptr}
        , m_stride{0ul} {}

    StridedBufferIterator(std::byte* pos, std::byte* end, std::size_t stride)
        : m_pos{pos}
        , m_end{end}
        , m_stride{stride} {}

    StridedBufferIterator(const StridedBufferIterator& other) = default;
    StridedBufferIterator(StridedBufferIterator&& other) = default;
    StridedBufferIterator& operator=(const StridedBufferIterator& other) = default;
    StridedBufferIterator& operator=(StridedBufferIterator&& other) = default;

    friend bool operator==(const StridedBufferIterator& a, const StridedBufferIterator& b) {
        return (a.m_pos == b.m_pos) || (a.m_end <= a.m_pos && b.m_end <= b.m_pos);
    }

    friend bool operator!=(const StridedBufferIterator& a, const StridedBufferIterator& b) {
        return !(a == b);
    }

    auto operator*() const -> value_type const& {
        return m_pos;
    }

    auto operator*() -> value_type const& {
        return m_pos;
    }

    auto operator->() const -> value_type const* {
        return &m_pos;
    }

    auto operator->() -> value_type* {
        return &m_pos;
    }

    auto operator++() -> iterator_type& {
        m_pos += m_stride;
        return *this;
    }

    auto operator++(int) -> iterator_type {
        auto copy{*this};
        ++(*this);
        return copy;
    }

    friend auto operator-(const iterator_type& a, const iterator_type& b) {
        // Expects(a.m_stride == b.m_stride);
        return (a.m_pos - b.m_pos) / a.m_stride;
    }
};
