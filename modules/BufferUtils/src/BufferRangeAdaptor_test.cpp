#include <BufferUtils/BufferRangeAdaptor.hpp>
#include <catch2/catch.hpp>

#include <vector>

TEST_CASE("Typed iteration with stride", "[BufferRangeAdaptor]") {
    GIVEN("an std::vector<int>") {
        std::vector<int> values{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        WHEN("we create a range view over the buffer of this vector, using BufferRangeAdaptor<int, sizeof(int) * 2>") {
            auto range = BufferRangeAdaptor<int, sizeof(int) * 2>(reinterpret_cast<std::byte*>(values.data()),
                                                                  values.size() / 2ul);
            THEN("iteration over this range view of the buffer produces ints from the original vector, but skipping "
                 "every second int") {
                auto it = values.begin();
                for (auto v : range) {
                    REQUIRE(*(it++) == v);
                    it++;
                }
            }
        }
    }
}
