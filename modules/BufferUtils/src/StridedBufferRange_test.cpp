#include <BufferUtils/StridedBufferRange.hpp>
#include <catch2/catch.hpp>

#include <vector>

TEST_CASE("Buffer iteration with stride", "[StridedBufferRange]") {
    GIVEN("a buffer") {
        std::vector<std::byte> buffer(10);
        WHEN("we create a StridedBufferRange from the buffer") {
            const auto stride = 8;
            auto range = StridedBufferRange(buffer.data(), buffer.size(), stride);
            THEN("Iteration over the StridedBufferRange produces pointers into the buffer with `stride` as spacing "
                 "between subsequent pointers") {
                std::size_t i{0ul};
                std::byte* base{buffer.data()};
                for (auto p : range) {
                    const auto expected = base + stride * (i++);
                    REQUIRE(expected == p);
                }
            }
        }
    }
}
