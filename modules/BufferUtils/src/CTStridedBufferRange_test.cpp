#include <BufferUtils/CTStridedBufferRange.hpp>
#include <catch2/catch.hpp>

#include <vector>

TEST_CASE("Buffer iteration with compile-time stride", "[CTStridedBufferRange]") {
    GIVEN("a buffer") {
        std::vector<std::byte> buffer(10);
        WHEN("we create a CTStridedBufferRange<Stride> from the buffer") {
            constexpr auto Stride = 8;
            auto range = CTStridedBufferRange<Stride>(buffer.data(), buffer.size());
            THEN("Iteration over the StridedBufferRange produces pointers into the buffer with `stride` as spacing "
                 "between subsequent pointers") {
                std::size_t i{0ul};
                std::byte* base{buffer.data()};
                for (auto p : range) {
                    const auto expected = base + Stride * (i++);
                    REQUIRE(expected == p);
                }
            }
        }
    }
}
