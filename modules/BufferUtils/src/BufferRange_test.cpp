#include <BufferUtils/BufferRange.hpp>
#include <catch2/catch.hpp>

#include <vector>

TEST_CASE("Buffer iteration", "[BufferRange]") {
    std::vector<std::byte> data(100);
    GIVEN("A buffer (with begin pointer and size)") {
        BufferRange range(data.data(), data.size());

        THEN("The iterator type is a std::byte*") {
            REQUIRE(std::is_same_v<decltype(range.begin()), std::byte*>);
        }

        THEN("We can use range based for loops to iterate over the buffer") {
            auto expectedPtr = data.data();
            for (const auto& val : range) {
                REQUIRE(&val == (expectedPtr++));
            }
        }
#if __has_include(<ranges>)
        THEN("We can use std::ranges::copy() to copy the data into a vector") {
            std::vector<std::byte> copy;
            std::ranges::copy(range, std::back_inserter(copy));
        }
#endif
        THEN("We can use std::copy() to copy the data into a vector") {
            std::vector<std::byte> copy;
            std::copy(std::begin(range), std::end(range), std::back_inserter(copy));
        }
    }
}