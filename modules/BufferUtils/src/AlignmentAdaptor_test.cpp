#include <BufferUtils/AlignmentAdaptor.hpp>
#include <catch2/catch.hpp>

#include <vector>

TEST_CASE("AlignmentAdaptor conversion operator", "[AlignmentAdaptor]") {
    GIVEN("a buffer") {
        std::vector<std::byte> buf(sizeof(std::uint32_t));
        WHEN("writing with big endian to the buffer") {
            AlignmentAdaptor<std::uint32_t, Alignment::Align64, Endian::Big> big{buf.data()};
            big = 0x112233AAu;
            AND_WHEN("the number is read again") {
                const auto read = big.get();
                THEN("the read number matches the one written") {
                    REQUIRE(read == 0x112233AAu);
                }
            }
        }
    }
}

TEST_CASE("AlignmentAdaptor BigEndian", "[AlignmentAdaptor]") {
    GIVEN("a buffer") {
        std::vector<std::byte> buf(sizeof(std::uint32_t));
        WHEN("writing with big endian to the buffer") {
            AlignmentAdaptor<std::uint32_t, Alignment::Align64, Endian::Big> big{buf.data()};
            big = 0x112233AAu;
            AND_WHEN("the number is read again") {
                const auto read = big.get();
                THEN("the read number matches the one written") {
                    REQUIRE(read == 0x112233AAu);
                }
            }
        }
    }
}

TEST_CASE("AlignmentAdaptor LittleEndian", "[AlignmentAdaptor]") {
    GIVEN("a buffer") {
        std::vector<std::byte> buf(sizeof(std::uint32_t));
        WHEN("writing with little endian to the buffer") {
            AlignmentAdaptor<std::uint32_t, Alignment::Align64, Endian::Little> little{buf.data()};
            little = 0x112233AAu;
            AND_WHEN("the number is read again") {
                const auto read = little.get();
                THEN("the read number matches the one written") {
                    REQUIRE(read == 0x112233AAu);
                }
            }
        }
    }
}

TEST_CASE("AlignmentAdaptor Endian Swap", "[AlignmentAdaptor]") {
    GIVEN("a buffer") {
        std::vector<std::byte> buf(sizeof(std::uint32_t));
        WHEN("writing with little endian to the buffer") {
            AlignmentAdaptor<std::uint32_t, Alignment::Align64, Endian::Little> little{buf.data()};
            little = 0x112233AAu;
            AND_WHEN("reading with big endian from the buffer") {
                AlignmentAdaptor<std::uint32_t, Alignment::Align64, Endian::Big> big{buf.data()};
                std::uint32_t resBig = big.get();
                THEN("the byte order of the read number is the reverse of the number written") {
                    REQUIRE(resBig == 0xAA332211u);
                }
            }
        }
        WHEN("writing with big endian to the buffer") {
            AlignmentAdaptor<std::uint32_t, Alignment::Align64, Endian::Big> big{buf.data()};
            big = 0x112233AAu;
            AND_WHEN("reading with little endian from the buffer") {
                AlignmentAdaptor<std::uint32_t, Alignment::Align64, Endian::Little> little{buf.data()};
                std::uint32_t resLittle = little.get();
                THEN("the byte order of the read number is the reverse of the number written") {
                    REQUIRE(resLittle == 0xAA332211u);
                }
            }
        }
    }
}
