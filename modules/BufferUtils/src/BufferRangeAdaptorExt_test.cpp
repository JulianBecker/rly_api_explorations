#include <BufferUtils/AlignmentAdaptor.hpp>
#include <BufferUtils/BufferRangeAdaptorExt.hpp>
#include <catch2/catch.hpp>

#include <vector>

TEST_CASE("2d access to 3d points", "[BufferRangeAdaptorExt]") {
    struct AccessAdaptor {
        std::byte* ptr;
    };

    std::vector<double> buf{1.1, 1.2, 1.3, 2.1, 2.2, 2.3, 3.1, 3.2, 3.3};

    BufferRangeAdaptorExt<AccessAdaptor, sizeof(double)> adaptedBuffer{
        reinterpret_cast<std::byte*>(buf.data()), reinterpret_cast<std::byte*>(buf.data() + buf.size())};

    std::size_t i{0ul};
    for (auto record : adaptedBuffer) {
        REQUIRE(record.ptr == reinterpret_cast<std::byte const*>(buf.data()) + sizeof(double) * i);

        ++i;
    }
}
