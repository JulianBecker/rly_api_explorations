#pragma once
#include <array>
#include <cctype>
#include <map>
#include <set>
#include <string>
#include <type_traits>
#include <utility>
#include <variant>
#include <vector>

namespace attrs {
struct Int {
    std::int32_t data;
};

struct Int8 {
    std::int8_t data;
};

struct UInt8 {
    std::uint8_t data;
};

struct Int16 {
    std::int16_t data;
};

struct UInt16 {
    std::uint16_t data;
};

struct Int32 {
    std::int32_t data;
};

struct UInt32 {
    std::uint32_t data;
};

struct Int64 {
    std::int64_t data;
};

struct UInt64 {
    std::uint64_t data;
};

struct Double {
    double data;
};

struct String {
    std::string data;
};

struct Bool {
    bool data;
};

struct Vec3d {
    std::array<double, 3ul> data;
};

struct Vec3df {
    std::array<float, 3ul> data;
};

struct Vec3us {
    std::array<std::uint16_t, 3ul> data;
};

struct Vec4d {
    std::array<double, 4ul> data;
};

struct Vec4df {
    std::array<float, 4ul> data;
};

struct SVec2dd {
    std::array<double, 2ul> data;
};

struct Vec2df {
    std::array<float, 2ul> data;
};

struct Vec2i {
    std::array<std::int32_t, 2ul> data;
};

struct Vec2ui {
    std::array<std::uint32_t, 2ul> data;
};

struct StringArray {
    std::vector<std::string> data;
};

struct StringSet {
    std::set<std::string> data;
};

struct StringMap {
    std::map<std::string, std::string> data;
};

struct Mat4df {
    std::array<float, 4ul * 4ul> data;
};

struct Mat4dd {
    std::array<double, 4ul * 4ul> data;
};

struct DVector {
    std::vector<double> data;
};

struct Vec3ddVector {
    std::vector<std::array<double, 3ul>> data;
};

struct Vec3dfVector {
    std::vector<std::array<float, 3ul>> data;
};

struct Vec4ddVector {
    std::vector<std::array<double, 4ul>> data;
};

struct VecPoly3d {
    std::vector<std::vector<std::array<double, 3ul>>> data;
};

struct Vec2ddVector {
    std::vector<std::array<double, 2ul>> data;
};

struct Vec2dfVector {
    std::vector<std::array<float, 2ul>> data;
};

struct IntVector {
    std::vector<std::int32_t> data;
};

struct UIntVector {
    std::vector<std::uint32_t> data;
};

struct Int8Vector {
    std::vector<std::int8_t> data;
};

struct UInt8Vector {
    std::vector<std::uint8_t> data;
};

struct Int16Vector {
    std::vector<std::int16_t> data;
};

struct UInt16Vector {
    std::vector<std::uint16_t> data;
};

struct Int32Vector {
    std::vector<std::int32_t> data;
};

struct UInt32Vector {
    std::vector<std::uint32_t> data;
};

struct Int64Vector {
    std::vector<std::int64_t> data;
};

struct UInt64Vector {
    std::vector<std::uint64_t> data;
};

struct Vec2iVector {
    std::vector<std::array<std::int32_t, 2ul>> data;
};

struct Vec2usVector {
    std::vector<std::array<std::uint16_t, 2ul>> data;
};

struct Mat4ddVector {
    std::vector<std::array<double, 4ul * 4ul>> data;
};

struct Uuid {
    std::string data;
};

struct Vec3uiVector {
    std::vector<std::array<std::uint32_t, 3ul>> data;
};

struct UuidSet {
    std::set<std::string> data;
};

struct ScanPt {
    std::array<std::uint8_t, 4ul> color;
    std::array<float, 3ul> position;
};

struct ScanPtVector {
    std::vector<ScanPt> data;
};

struct UuidVector {
    std::vector<Uuid> data;
};

struct BoolVector {
    std::vector<bool> data;
};

struct JsonValue {
    std::string data;
};

struct Unknown {
    std::string data;
};

using Attr =
    std::variant<Int, Int8, UInt8, Int16, UInt16, Int32, UInt32, Int64, UInt64, Double, String, Bool, Vec3d, Vec3df,
                 Vec3us, Vec4d, Vec4df, SVec2dd, Vec2df, Vec2i, Vec2ui, StringArray, StringSet, StringMap, Mat4df,
                 Mat4dd, DVector, Vec3ddVector, Vec3dfVector, Vec4ddVector, VecPoly3d, Vec2ddVector, Vec2dfVector,
                 IntVector, UIntVector, Int8Vector, UInt8Vector, Int16Vector, UInt16Vector, Int32Vector, UInt32Vector,
                 Int64Vector, UInt64Vector, Vec2iVector, Vec2usVector, Mat4ddVector, Uuid, Vec3uiVector, UuidSet,
                 ScanPt, ScanPtVector, UuidVector, BoolVector, JsonValue, Unknown>;

struct AttrContainer final {
    std::map<std::string, AttrContainer> fields;
    std::map<std::string, Attr> attributes;

    auto operator[](const std::string& attrName) const -> AttrContainer const& {
        return fields.at(attrName);
    }

    template<typename AttrType>
    auto get(const std::string& attrName) const -> AttrType const& {
        return std::get<AttrType>(attributes.at(attrName));
    }
};

} // namespace attrs
