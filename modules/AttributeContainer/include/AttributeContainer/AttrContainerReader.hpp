#pragma once
#include "AttrContainer.hpp"

#include <map>
#include <string>
#include <utility>
#include <vector>

class AttrContainerParseHandler;

class AttrContainerReader {
    using iterator = char const*;
    using attr_reader_t = std::pair<iterator, attrs::Attr> (*)(iterator, iterator);
    using attr_empty_t = attrs::Attr (*)();

    struct Factory {
        attr_reader_t read;
        attr_empty_t empty;
    };

private:
    static std::map<std::string, Factory> m_factories;

    auto state_BeginContainer(iterator start, iterator end, AttrContainerParseHandler& builder) -> iterator;
    auto state_BeginSubContainer(iterator start, iterator end, AttrContainerParseHandler& builder) -> iterator;
    auto state_EndContainer(iterator start, iterator end, AttrContainerParseHandler& builder) -> iterator;
    auto state_EndSubContainer(iterator start, iterator end, AttrContainerParseHandler& builder) -> iterator;
    auto state_ReadAttr(iterator start, iterator end, AttrContainerParseHandler& builder) -> iterator;

    using state_type = auto (AttrContainerReader::*)(iterator start, iterator end, AttrContainerParseHandler& builder)
                           -> iterator;
    state_type m_state{&AttrContainerReader::state_BeginContainer};

public:
    auto parse(iterator start, iterator end) -> attrs::AttrContainer;
};
