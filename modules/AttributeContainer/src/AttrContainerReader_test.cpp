
#include <AttributeContainer/AttrContainerReader.hpp>
#include <catch2/catch.hpp>

#include <iterator>
#include <sstream>

TEST_CASE("AttrContainerReader test", "[AttrContainerReader]") {
    GIVEN("a serialized Attribute Container with one string attribute") {
        const std::string serialized(R"DATA(
            AttrContainer {
                Attr<String> {
                    Name "Name"
                    Value "ScannerParam"
                }
            }
        )DATA");

        WHEN("the container is deserialized") {
            auto cont = AttrContainerReader{}.parse(serialized.data(), serialized.data() + serialized.length());
            THEN("we find one string attribute") {
                REQUIRE(cont.attributes.size() == 1ul);
                REQUIRE(cont.get<attrs::String>("Name").data == "ScannerParam");
            }
        }
    }
}
