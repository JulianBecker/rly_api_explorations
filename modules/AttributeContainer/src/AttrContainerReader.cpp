#include <AttributeContainer/AttrContainer.hpp>
#include <AttributeContainer/AttrContainerReader.hpp>

#include <charconv>
#include <sstream>
#include <stack>

static inline bool isWhitespace(char const ch) {
    return ch == ' ' || ch == '\n' || ch == '\r' || ch == '\t';
}

static constexpr bool isKeywordChar(char const ch) {
    return ('a' <= ch && ch <= 'z') || ('A' <= ch && ch <= 'Z') || ('0' <= ch && ch <= '9') || ch == '_';
}

static constexpr bool isOpenBrace(char const ch) {
    return ch == '{';
}

static constexpr bool isClosingBrace(char const ch) {
    return ch == '}';
}

template<typename InputIterator, typename Pred>
static auto skipUntil(InputIterator start, InputIterator end, Pred&& pred) -> InputIterator {
    for (; start != end; ++start) {
        if (pred(*start))
            return start;
    }
    return end;
}

template<typename InputIterator, typename Pred, typename = std::enable_if_t<std::is_invocable_v<Pred, char>>>
static auto skipUntilAfter(InputIterator start, InputIterator end, Pred&& pred) -> InputIterator {
    if ((start = skipUntil(start, end, pred)) == end)
        return end;
    return std::next(start);
}

template<typename InputIterator>
static auto skipUntilAfter(InputIterator start, InputIterator end, char ch) -> InputIterator {
    const auto matchesChar = [ch](char chIn) { return chIn == ch; };
    if ((start = skipUntil(start, end, matchesChar)) == end)
        return end;
    return std::next(start);
}

template<typename InputIterator, typename Pred>
static auto takeWhile(InputIterator start, InputIterator end, Pred&& pred) -> std::pair<InputIterator, std::string> {
    for (auto newStart{start}; newStart != end; ++newStart) {
        if (!pred(*newStart))
            return {newStart,
                    std::string(reinterpret_cast<char const*>(start), reinterpret_cast<char const*>(newStart))};
    }
    return {end, {}};
}

template<typename InputIterator>
static auto skipWS(InputIterator start, InputIterator end) -> InputIterator {
    return skipUntil(start, end, [](const auto& ch) { return !isWhitespace(ch); });
}

template<typename InputIterator>
static auto takeKeyword(InputIterator start, InputIterator end) -> std::pair<InputIterator, std::string> {
    if ((start = skipWS(start, end)) == end)
        return {end, {}};
    return takeWhile(start, end, isKeywordChar);
}

template<typename InputIterator>
static auto skipAfterKeyword(InputIterator start, InputIterator end, const std::string& keyword) -> InputIterator {
    auto [newStart, readWord] = takeKeyword(start, end);
    if (keyword == readWord)
        return newStart;
    return end;
}

template<typename InputIterator>
static auto takeQuotedString(InputIterator start, InputIterator end) -> std::pair<InputIterator, std::string> {
    if ((start = skipUntilAfter(start, end, '"')) == end)
        return {end, {}};
    if (*start == '"')
        return {std::next(start), {}};

    auto first{start};
    char chPrev{*(start++)};
    for (; *start != '"' || chPrev == '\\'; chPrev = *(start++))
        ;
    std::string result(reinterpret_cast<char const*>(first), reinterpret_cast<char const*>(start));

    if ((start = skipUntilAfter(start, end, '"')) == end)
        return {end, {}};

    return {start, result};
}

template<typename PrimitiveType, typename InputIterator>
static auto takePrimitiveType(InputIterator start, InputIterator end) -> std::pair<InputIterator, PrimitiveType> {
    if ((start = skipWS(start, end)) == end)
        return {end, {}};

    std::string valueStr;
    std::tie(start, valueStr) = takeWhile(start, end, [](char ch) { return !isWhitespace(ch); });
    if (start == end)
        return {end, {}};
    PrimitiveType val{};
    if constexpr (std::is_integral_v<PrimitiveType> && sizeof(PrimitiveType) < sizeof(int)) {
        int tmp{};
        std::from_chars(valueStr.c_str(), valueStr.c_str() + valueStr.length(), tmp);
        val = static_cast<PrimitiveType>(tmp);
    } else if constexpr (std::is_same_v<PrimitiveType, double>) {
        // apple-clang doesn't support std::from_chars() for floating point
        val = std::stod(valueStr);
    } else if constexpr (std::is_same_v<PrimitiveType, float>) {
        // apple-clang doesn't support std::from_chars() for floating point
        val = std::stof(valueStr);
    } else {
        std::from_chars(valueStr.c_str(), valueStr.c_str() + valueStr.length(), val);
    }
    return {start, val};
}

template<typename InputIterator>
static auto enterBlock(InputIterator start, InputIterator end) -> InputIterator {
    return skipUntilAfter(start, end, isOpenBrace);
}

template<typename InputIterator>
static auto exitBlock(InputIterator start, InputIterator end) -> InputIterator {
    return skipUntilAfter(start, end, isClosingBrace);
}

template<typename>
struct AttrReader;

template<typename AttrType, typename PrimitiveType>
struct AttrReaderPrimitive {
    static auto empty() -> attrs::Attr {
        return AttrType{0};
    }

    template<typename InputIterator>
    static auto read(InputIterator start, InputIterator end) -> std::pair<InputIterator, attrs::Attr> {
        const auto [newStart, val] = takePrimitiveType<PrimitiveType>(start, end);
        return {newStart, AttrType{val}};
    }
};

template<>
struct AttrReader<attrs::Uuid> {
    static auto empty() -> attrs::Attr {
        return attrs::Uuid{{"00000000-0000-0000-0000-000000000000"}};
    }

    template<typename InputIterator>
    static auto read(InputIterator start, InputIterator end) -> std::pair<InputIterator, attrs::Attr> {
        if ((start = skipWS(start, end)) == end)
            return {end, {}};

        std::string value;
        std::tie(start, value) = takeWhile(start, end, [](char ch) { return !isWhitespace(ch); });
        return {start, attrs::Uuid{value}};
    }
};

template<>
struct AttrReader<attrs::String> {
    static auto empty() -> attrs::Attr {
        return attrs::String{};
    }

    template<typename InputIterator>
    static auto read(InputIterator start, InputIterator end) -> std::pair<InputIterator, attrs::Attr> {
        std::string value;
        std::tie(start, value) = takeQuotedString(start, end);
        return {start, attrs::String{value}};
    }
};

template<typename AttrType, typename PrimitiveType, std::size_t N>
struct AttrReaderVecNdT {
    static auto empty() -> attrs::Attr {
        return AttrType{};
    }

    template<typename InputIterator>
    static auto read(InputIterator start, InputIterator end) -> std::pair<InputIterator, attrs::Attr> {
        AttrType attr{};
        for (std::size_t i{0ul}; i < N; ++i)
            std::tie(start, attr.data[i]) = takePrimitiveType<PrimitiveType>(start, end);

        return {start, AttrType{attr}};
    }
};

template<typename AttrType, typename PrimitiveType>
struct AttrReaderTVector {
    static auto empty() -> attrs::Attr {
        return AttrType{};
    }

    template<typename InputIterator>
    static auto read(InputIterator start, InputIterator end) -> std::pair<InputIterator, attrs::Attr> {
        if ((start = skipUntilAfter(start, end, '{')) == end)
            return {end, AttrType{}};

        std::vector<PrimitiveType> data;
        while (start != end) {
            PrimitiveType entry;
            std::tie(start, entry) = takePrimitiveType<PrimitiveType>(start, end);
            data.push_back(std::move(entry));
            if ((start = skipWS(start, end)) == end)
                return {end, {}};
            if (*start == '}')
                break;
        }

        return {skipUntilAfter(start, end, '}'), AttrType{data}};
    }
};

template<typename AttrType, typename PrimitiveType, std::size_t N>
struct AttrReaderVecNTVector {
    static auto empty() -> attrs::Attr {
        return AttrType{};
    }

    template<typename InputIterator>
    static auto read(InputIterator start, InputIterator end) -> std::pair<InputIterator, attrs::Attr> {
        if ((start = skipUntilAfter(start, end, '{')) == end)
            return {end, {}};

        std::vector<std::array<PrimitiveType, N>> data;
        while (start != end) {
            std::array<PrimitiveType, N> entry;
            for (std::size_t i{0u}; i < N; ++i)
                std::tie(start, entry[i]) = takePrimitiveType<PrimitiveType>(start, end);
            data.push_back(std::move(entry));
            if ((start = skipWS(start, end)) == end)
                return {end, AttrType{}};
            if (*start == '}')
                break;
        }

        return {skipUntilAfter(start, end, '}'), AttrType{data}};
    }
};

template<>
struct AttrReader<attrs::StringArray> {
    static auto empty() -> attrs::Attr {
        return attrs::StringArray{};
    }

    template<typename InputIterator>
    static auto read(InputIterator start, InputIterator end) -> std::pair<InputIterator, attrs::Attr> {
        std::uint32_t numEntries{};
        std::tie(start, numEntries) = takePrimitiveType<std::uint32_t>(start, end);

        std::vector<std::string> data;
        data.reserve(numEntries);
        while (numEntries-- > 0 && start != end) {
            std::string entry;
            std::tie(start, entry) = takeQuotedString(start, end);
            data.push_back(std::move(entry));
            if ((start = skipWS(start, end)) == end)
                return {end, attrs::StringArray{}};
            if (*start == '}')
                break;
        }
        return {start, attrs::StringArray{data}};
    }
};

template<>
struct AttrReader<attrs::Int> : AttrReaderPrimitive<attrs::Int, std::int32_t> {};
template<>
struct AttrReader<attrs::Int8> : AttrReaderPrimitive<attrs::Int8, std::int8_t> {};
template<>
struct AttrReader<attrs::UInt8> : AttrReaderPrimitive<attrs::UInt8, std::uint8_t> {};
template<>
struct AttrReader<attrs::Int16> : AttrReaderPrimitive<attrs::Int16, std::int16_t> {};
template<>
struct AttrReader<attrs::UInt16> : AttrReaderPrimitive<attrs::UInt16, std::uint16_t> {};
template<>
struct AttrReader<attrs::Int32> : AttrReaderPrimitive<attrs::Int32, std::int32_t> {};
template<>
struct AttrReader<attrs::UInt32> : AttrReaderPrimitive<attrs::UInt32, std::uint32_t> {};
template<>
struct AttrReader<attrs::Int64> : AttrReaderPrimitive<attrs::Int64, std::int64_t> {};
template<>
struct AttrReader<attrs::UInt64> : AttrReaderPrimitive<attrs::UInt64, std::uint64_t> {};
template<>
struct AttrReader<attrs::Double> : AttrReaderPrimitive<attrs::Double, double> {};
template<>
struct AttrReader<attrs::Bool> : AttrReaderPrimitive<attrs::Bool, bool> {};

template<>
struct AttrReader<attrs::SVec2dd> : AttrReaderVecNdT<attrs::SVec2dd, double, 2ul> {};
template<>
struct AttrReader<attrs::Vec2df> : AttrReaderVecNdT<attrs::Vec2df, float, 2ul> {};
template<>
struct AttrReader<attrs::Vec3d> : AttrReaderVecNdT<attrs::Vec3d, double, 3ul> {};
template<>
struct AttrReader<attrs::Vec3us> : AttrReaderVecNdT<attrs::Vec3us, std::uint16_t, 3ul> {};
template<>
struct AttrReader<attrs::Vec4d> : AttrReaderVecNdT<attrs::Vec4d, double, 4ul> {};
template<>
struct AttrReader<attrs::Mat4dd> : AttrReaderVecNdT<attrs::Mat4dd, double, 16ul> {};

template<>
struct AttrReader<attrs::DVector> : AttrReaderTVector<attrs::DVector, double> {};
template<>
struct AttrReader<attrs::IntVector> : AttrReaderTVector<attrs::IntVector, std::int32_t> {};
template<>
struct AttrReader<attrs::UInt8Vector> : AttrReaderTVector<attrs::UInt8Vector, std::uint8_t> {};
template<>
struct AttrReader<attrs::UInt32Vector> : AttrReaderTVector<attrs::UInt32Vector, std::uint32_t> {};

template<>
struct AttrReader<attrs::Vec2iVector> : AttrReaderVecNTVector<attrs::Vec2iVector, std::int32_t, 2ul> {};
template<>
struct AttrReader<attrs::Vec2ddVector> : AttrReaderVecNTVector<attrs::Vec2ddVector, double, 2ul> {};
template<>
struct AttrReader<attrs::Vec3uiVector> : AttrReaderVecNTVector<attrs::Vec3uiVector, std::uint32_t, 3ul> {};
template<>
struct AttrReader<attrs::Vec3ddVector> : AttrReaderVecNTVector<attrs::Vec3ddVector, double, 3ul> {};
template<>
struct AttrReader<attrs::Vec4ddVector> : AttrReaderVecNTVector<attrs::Vec4ddVector, double, 4ul> {};

class AttrContainerParseHandler {
    std::stack<std::pair<attrs::AttrContainer, std::string /*fieldName*/>> m_currentFields;

public:
    AttrContainerParseHandler() {
        m_currentFields.emplace(attrs::AttrContainer{}, std::string{});
    }

    void onAttr(const std::string& name, const attrs::Attr& attr) {
        m_currentFields.top().first.attributes.emplace(name, attr);
    }

    void onFieldBegin(const std::string& className, const std::string& baseClass) {
        m_currentFields.emplace(attrs::AttrContainer{}, std::string{});
    }

    void onFieldName(const std::string& fieldName) {
        m_currentFields.top().second = fieldName;
    }

    void onFieldComplete() {
        auto [field, name] = std::move(m_currentFields.top());
        m_currentFields.pop();
        m_currentFields.top().first.fields.emplace(name, std::move(field));
    }

    auto result() -> attrs::AttrContainer {
        return m_currentFields.top().first;
    }
};

auto AttrContainerReader::state_BeginContainer(AttrContainerReader::iterator start, AttrContainerReader::iterator end,
                                               AttrContainerParseHandler& builder) -> iterator {
    if ((start = skipAfterKeyword(start, end, "AttrContainer")) == end)
        return end;
    if (start == end)
        return end;
    if ((start = enterBlock(start, end)) == end)
        return end;
    if ((start = skipWS(start, end)) == end)
        return end;
    if (isClosingBrace(*start))
        m_state = &AttrContainerReader::state_EndContainer;
    else
        m_state = &AttrContainerReader::state_ReadAttr;

    return start;
}

auto AttrContainerReader::state_BeginSubContainer(iterator start, iterator end, AttrContainerParseHandler& builder)
    -> iterator {
    if ((start = skipAfterKeyword(start, end, "SubContainer")) == end)
        return end;
    if (start == end)
        return end;
    if ((start = enterBlock(start, end)) == end)
        return end;
    if ((start = skipWS(start, end)) == end)
        return end;

    std::string className;
    std::tie(start, className) = takeQuotedString(start, end);
    if (start == end)
        return end;
    std::string baseClassName;
    std::tie(start, baseClassName) = takeQuotedString(start, end);
    if (start == end)
        return end;

    builder.onFieldBegin(className, baseClassName);
    m_state = &AttrContainerReader::state_BeginContainer;

    return start;
}

auto AttrContainerReader::state_EndContainer(iterator start, iterator end, AttrContainerParseHandler& builder)
    -> iterator {
    if ((start = exitBlock(start, end)) == end)
        return end;
    if ((start = skipWS(start, end)) == end)
        return end;
    if (isClosingBrace(*start))
        m_state = &AttrContainerReader::state_EndSubContainer;
    else
        m_state = &AttrContainerReader::state_ReadAttr;
    return start;
}

auto AttrContainerReader::state_EndSubContainer(iterator start, iterator end, AttrContainerParseHandler& builder)
    -> iterator {
    if ((start = exitBlock(start, end)) == end)
        return end;
    if ((start = skipWS(start, end)) == end)
        return end;

    builder.onFieldComplete();

    if (isClosingBrace(*start))
        m_state = &AttrContainerReader::state_EndContainer;
    else if (*start == 'S')
        m_state = &AttrContainerReader::state_BeginSubContainer;
    else
        m_state = &AttrContainerReader::state_ReadAttr;
    return start;
}

auto AttrContainerReader::state_ReadAttr(iterator start, iterator end, AttrContainerParseHandler& builder) -> iterator {
    if ((start = skipAfterKeyword(start, end, "Attr")) == end)
        return end;
    if ((start = skipUntilAfter(start, end, '<')) == end)
        return end;

    std::string attrType;
    std::tie(start, attrType) = takeKeyword(start, end);
    if (start == end)
        return end;
    if ((start = skipUntilAfter(start, end, '>')) == end)
        return end;
    if ((start = enterBlock(start, end)) == end)
        return end;

    if ((start = skipAfterKeyword(start, end, "Name")) == end)
        return end;

    std::string attrName;
    std::tie(start, attrName) = takeQuotedString(start, end);
    if (start == end)
        return end;

    if ((start = skipWS(start, end)) == end)
        return end;

    auto const& factory = m_factories.at(attrType);
    attrs::Attr attr;
    if (*start == '}')
        attr = factory.empty();
    else {
        if ((start = skipAfterKeyword(start, end, "Value")) == end)
            return end;
        std::tie(start, attr) = factory.read(start, end);
    }

    builder.onAttr(attrName, attr);
    if (attrName == "Name" && std::holds_alternative<attrs::String>(attr))
        builder.onFieldName(std::get<attrs::String>(attr).data);

    if ((start = exitBlock(start, end)) == end)
        return end;
    if ((start = skipWS(start, end)) == end)
        return end;
    if (isClosingBrace(*start))
        m_state = &AttrContainerReader::state_EndContainer;
    else if (*start == 'S')
        m_state = &AttrContainerReader::state_BeginSubContainer;
    else
        m_state = &AttrContainerReader::state_ReadAttr;
    return start;
}

auto AttrContainerReader::parse(iterator start, iterator end) -> attrs::AttrContainer {
    AttrContainerParseHandler handler;

    while (start != end)
        start = (this->*m_state)(start, end, handler);
    return handler.result();
}

std::map<std::string, AttrContainerReader::Factory> AttrContainerReader::m_factories{
    {"String", {&AttrReader<attrs::String>::read<AttrContainerReader::iterator>, &AttrReader<attrs::String>::empty}},
    {"int", {&AttrReader<attrs::Int>::read<AttrContainerReader::iterator>, &AttrReader<attrs::Int>::empty}},
    {"int8_t", {&AttrReader<attrs::Int8>::read<AttrContainerReader::iterator>, &AttrReader<attrs::Int8>::empty}},
    {"uint8_t", {&AttrReader<attrs::UInt8>::read<AttrContainerReader::iterator>, &AttrReader<attrs::UInt8>::empty}},
    {"int16_t", {&AttrReader<attrs::Int16>::read<AttrContainerReader::iterator>, &AttrReader<attrs::Int16>::empty}},
    {"uint16_t", {&AttrReader<attrs::UInt16>::read<AttrContainerReader::iterator>, &AttrReader<attrs::UInt16>::empty}},
    {"int32_t", {&AttrReader<attrs::Int32>::read<AttrContainerReader::iterator>, &AttrReader<attrs::Int32>::empty}},
    {"uint32_t", {&AttrReader<attrs::UInt32>::read<AttrContainerReader::iterator>, &AttrReader<attrs::UInt32>::empty}},
    {"int64_t", {&AttrReader<attrs::Int64>::read<AttrContainerReader::iterator>, &AttrReader<attrs::Int64>::empty}},
    {"uint64_t", {&AttrReader<attrs::UInt64>::read<AttrContainerReader::iterator>, &AttrReader<attrs::UInt64>::empty}},
    {"double", {&AttrReader<attrs::Double>::read<AttrContainerReader::iterator>, &AttrReader<attrs::Double>::empty}},
    {"String", {&AttrReader<attrs::String>::read<AttrContainerReader::iterator>, &AttrReader<attrs::String>::empty}},
    {"bool", {&AttrReader<attrs::Bool>::read<AttrContainerReader::iterator>, &AttrReader<attrs::Bool>::empty}},
    {"Uuid", {&AttrReader<attrs::Uuid>::read<AttrContainerReader::iterator>, &AttrReader<attrs::Uuid>::empty}},
    {"SVec2dd", {&AttrReader<attrs::SVec2dd>::read<AttrContainerReader::iterator>, &AttrReader<attrs::SVec2dd>::empty}},
    {"SVec2df", {&AttrReader<attrs::Vec2df>::read<AttrContainerReader::iterator>, &AttrReader<attrs::Vec2df>::empty}},
    {"SVec3us", {&AttrReader<attrs::Vec3us>::read<AttrContainerReader::iterator>, &AttrReader<attrs::Vec3us>::empty}},
    {"Vec3dd", {&AttrReader<attrs::Vec3d>::read<AttrContainerReader::iterator>, &AttrReader<attrs::Vec3d>::empty}},
    {"Vec4dd", {&AttrReader<attrs::Vec4d>::read<AttrContainerReader::iterator>, &AttrReader<attrs::Vec4d>::empty}},
    {"DVector", {&AttrReader<attrs::DVector>::read<AttrContainerReader::iterator>, &AttrReader<attrs::DVector>::empty}},
    {"UInt8Vector",
     {&AttrReader<attrs::UInt8Vector>::read<AttrContainerReader::iterator>, &AttrReader<attrs::UInt8Vector>::empty}},
    {"UInt32Vector",
     {&AttrReader<attrs::UInt32Vector>::read<AttrContainerReader::iterator>, &AttrReader<attrs::UInt32Vector>::empty}},
    {"Vec2ddVector",
     {&AttrReader<attrs::Vec2ddVector>::read<AttrContainerReader::iterator>, &AttrReader<attrs::Vec2ddVector>::empty}},
    {"Vec2iVector",
     {&AttrReader<attrs::Vec2iVector>::read<AttrContainerReader::iterator>, &AttrReader<attrs::Vec2iVector>::empty}},
    {"Vec3ddVector",
     {&AttrReader<attrs::Vec3ddVector>::read<AttrContainerReader::iterator>, &AttrReader<attrs::Vec3ddVector>::empty}},
    {"Vec3uiVector",
     {&AttrReader<attrs::Vec3uiVector>::read<AttrContainerReader::iterator>, &AttrReader<attrs::Vec3uiVector>::empty}},
    {"Vec4ddVector",
     {&AttrReader<attrs::Vec4ddVector>::read<AttrContainerReader::iterator>, &AttrReader<attrs::Vec4ddVector>::empty}},
    {"IntVector",
     {&AttrReader<attrs::IntVector>::read<AttrContainerReader::iterator>, &AttrReader<attrs::IntVector>::empty}},
    {"StringArray",
     {&AttrReader<attrs::StringArray>::read<AttrContainerReader::iterator>, &AttrReader<attrs::StringArray>::empty}},
    {"Mat4dd", {&AttrReader<attrs::Mat4dd>::read<AttrContainerReader::iterator>, &AttrReader<attrs::Mat4dd>::empty}}};
