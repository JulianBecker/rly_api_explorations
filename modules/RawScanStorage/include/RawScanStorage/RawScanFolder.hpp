#pragma once
#include "AnglesCompensation.hpp"
#include "CompensationParamsBuilder.hpp"
#include "MifParser.hpp"
#include "ReverseBlockedRange.hpp"
#include "ScanDataDecompressor.hpp"
#include "ScanDataStreamIterator.hpp"
#include "ScannerCompensationParams.hpp"

#include <AttributeContainer/AttrContainerReader.hpp>
#include <FileIO/MemoryMappedFile.hpp>

#include <Eigen/Eigen>
#include <algorithm>
#include <charconv>
#include <filesystem>
#include <optional>
#include <regex>
#include <string>
#include <utility>
#include <vector>

static auto readParams(const std::string& paramFileName) -> attrs::AttrContainer {
    auto file = MemoryMappedFile{paramFileName};
    AttrContainerReader reader;
    return reader.parse(reinterpret_cast<char const*>(file.begin()), reinterpret_cast<char const*>(file.end()));
}

class ScanDataStreamRange {
    std::shared_ptr<std::vector<MemoryMappedFile>> m_loaders;

public:
    ScanDataStreamRange(std::shared_ptr<std::vector<MemoryMappedFile>> loaders)
        : m_loaders{std::move(loaders)} {}

    auto begin() const -> ScanDataStreamIterator {
        return {m_loaders, 0ul};
    }

    auto end() const -> ScanDataStreamIterator {
        return {m_loaders, m_loaders->size()};
    }
};

class ScanDataStream {
    std::shared_ptr<std::vector<MemoryMappedFile>> m_mappedFiles;

public:
    ScanDataStream(std::vector<std::filesystem::path> scanDataStreamPaths) {
        m_mappedFiles = std::make_shared<std::vector<MemoryMappedFile>>();
        m_mappedFiles->reserve(scanDataStreamPaths.size());
        std::transform(scanDataStreamPaths.begin(), scanDataStreamPaths.end(), std::back_inserter(*m_mappedFiles),
                       [](const auto& path) -> MemoryMappedFile { return {path}; });
    }

    auto packets() const -> ScanDataStreamRange {
        return {m_mappedFiles};
    }
};

class CompensatedScanDataStreamIterator {
    std::array<char, 3> CSE{'C', 'S', 'E'};
    std::array<char, 3> MIF{'M', 'I', 'F'};
    MIFParser m_parseMif;
    ScanDataDecompressor m_decompressor;
    ScannerCompensationParams m_params;
    ScanDataStreamIterator m_iter;
    ScanDataStreamIterator m_iterEnd;
    std::optional<ReverseBlockedRange> m_blockRange;
    ReverseBlockIterator m_blockIter;
    ReverseBlockIterator m_blockIterEnd;
    FblPacketRef m_packet;
    std::pair<Eigen::Vector4d, std::uint16_t> m_scanPt;

    bool moveNext() {
        for (; m_iter != m_iterEnd; ++m_iter) {
            if (m_blockRange) {
                for (; m_blockIter != m_blockIterEnd; ++m_blockIter) {
                    if (m_decompressor.process(*m_blockIter)) {
                        if (m_decompressor.getFilter())
                            continue;
                        m_scanPt.first = applyAnglesCompensation(
                            m_params, m_parseMif.HEnc_value, m_decompressor.getIndex(), m_decompressor.getDistance());
                        m_scanPt.second = m_decompressor.getGrayValue();
                        ++m_blockIter;
                        return true;
                    }
                }
            }

            for (; m_iter != m_iterEnd; ++m_iter) {
                if (m_iter->cmdId == MIF) {
                    m_decompressor.reset();
                    const std::size_t numWords = m_iter->payloadSize / sizeof(std::uint32_t);
                    for (std::size_t i = 0; i < numWords; i++) {
                        std::uint32_t word;
                        std::memcpy(&word, m_iter->payload + i * sizeof(std::uint32_t), sizeof(std::uint32_t));
                        m_parseMif.processWord(word);
                    }
                } else if (m_iter->cmdId == CSE) {
                    m_packet = *m_iter;
                    m_blockRange =
                        ReverseBlockedRange(16ul, (std::uint8_t const*) m_packet.payload, m_packet.payloadSize);
                    m_blockIter = m_blockRange->begin();
                    m_blockIterEnd = m_blockRange->end();
                    break;
                }
            }
        }
        m_blockRange = std::nullopt;
        m_blockIter = ReverseBlockIterator{};
        m_blockIterEnd = ReverseBlockIterator{};
        return false;
    };

public:
    CompensatedScanDataStreamIterator(ScanDataStreamIterator iter, ScanDataStreamIterator iterEnd,
                                      ScannerCompensationParams params)
        : m_iter{std::move(iter)}
        , m_iterEnd{std::move(iterEnd)}
        , m_params{params}
        , m_blockRange{std::nullopt} {
        moveNext();
    }

    friend bool operator==(const CompensatedScanDataStreamIterator& a, const CompensatedScanDataStreamIterator& b) {
        return a.m_iter == b.m_iter && a.m_blockIter == b.m_blockIter;
    }

    friend bool operator!=(const CompensatedScanDataStreamIterator& a, const CompensatedScanDataStreamIterator& b) {
        return !(a == b);
    }

    auto operator*() const -> std::pair<Eigen::Vector4d, std::uint16_t> const& {
        return m_scanPt;
    }

    auto operator->() const -> std::pair<Eigen::Vector4d, std::uint16_t> const* {
        return &m_scanPt;
    }

    auto operator++() -> CompensatedScanDataStreamIterator& {
        moveNext();
        return *this;
    }

    auto operator++(int) -> CompensatedScanDataStreamIterator {
        auto copy(*this);
        ++(*this);
        return copy;
    }
};

class CompensatedScanDataStream {
    ScanDataStreamRange m_scanData;
    ScannerCompensationParams m_params;

public:
    CompensatedScanDataStream(ScanDataStreamRange scanData, ScannerCompensationParams params)
        : m_scanData{std::move(scanData)}
        , m_params{std::move(params)} {}

    auto begin() const -> CompensatedScanDataStreamIterator {
        return {m_scanData.begin(), m_scanData.end(), m_params};
    }

    auto end() const -> CompensatedScanDataStreamIterator {
        return {m_scanData.end(), m_scanData.end(), m_params};
    }
};

auto streamIndexFromFileName(std::string const& fileName) -> std::optional<std::size_t>;

template<typename Begin, typename End>
static inline void sortScanDataStreams(Begin&& begin, End&& end) {
    std::sort(begin, end, [](const auto& a, const auto& b) {
        const auto indexA = streamIndexFromFileName(a.filename().string());
        const auto indexB = streamIndexFromFileName(b.filename().string());
        if (!indexA || !indexB)
            return true;
        return *indexA < *indexB;
    });
}

auto findScanDataStreams(const std::filesystem::path& rawScanFolderPath) -> std::vector<std::filesystem::path>;

class ScanFolder {
    std::filesystem::path m_path;

    auto getParamPath() const -> std::filesystem::path {
        return m_path / "Scans" / "1" / "Params";
    }

    auto getScanDataStreamFolderPath() const -> std::filesystem::path {
        return m_path / "Scans" / "1";
    }

    auto getScanDataStream() const -> ScanDataStream {
        return ScanDataStream(findScanDataStreams(getScanDataStreamFolderPath()));
    }

public:
    ScanFolder(std::filesystem::path path)
        : m_path{std::move(path)} {}

    auto points() const -> CompensatedScanDataStream {
        return {getScanDataStream().packets(), extractCompensationParams(readParams("assets/Params"))};
    }
};