#pragma once
#include <cstdint>

class ScanDataDecompressor
{
    enum class State
    {
        DECOMP_STATE_DUMMY,
        DECOMP_STATE_DIST,
        DECOMP_STATE_DIST2,
        DECOMP_STATE_DIST3,
        DECOMP_STATE_DIST4,
        DECOMP_STATE_GRAY,
        DECOMP_STATE_GRAY2,
    } m_state{State::DECOMP_STATE_DIST};

    std::uint64_t m_distance{0ul};
    std::int64_t m_distanceDelta{0ul};
    std::uint16_t m_grayValue{0u};
    std::int16_t m_grayValueDelta{0u};
    bool m_sync{false};
    bool m_filter{false};
    std::size_t m_index{0ul};
    std::size_t m_counter{0ul};

    auto handleDummy(std::uint8_t data) -> State
    {
        return data == 0x00u ? State::DECOMP_STATE_DUMMY : State::DECOMP_STATE_DIST;
    }
    auto handleDist(std::uint8_t data) -> State
    {
        switch (static_cast<std::uint32_t>(data >> 6u))
        {
        case 1u:
            m_distanceDelta = ((data & 0x20u) != 0u ? -0x20 + (std::int64_t)(data & 0x1Fu) : (std::int64_t)(data & 0x1Fu)) << 0u;
            return State::DECOMP_STATE_GRAY;
        case 2u:
            m_distanceDelta = ((data & 0x20u) != 0u ? -0x20 + (std::int64_t)(data & 0x1Fu) : (std::int64_t)(data & 0x1Fu)) << 8u;
            return State::DECOMP_STATE_DIST2;
        case 3u:
            m_distanceDelta = ((data & 0x20u) != 0u ? -0x20 + (std::int64_t)(data & 0x1Fu) : (std::int64_t)(data & 0x1Fu)) << 16u;
            return State::DECOMP_STATE_DIST3;
        default:
            break;
        }
        if ((data & 0xD8u) == 0x10u)
        {
            m_distanceDelta = ((data & 0x20u) != 0u ? -0x08 + (std::int64_t)(data & 0x07u) : (std::int64_t)(data & 0x07u)) << 24u;
            return State::DECOMP_STATE_DIST4;
        }
        else if (data == 0u)
        {
            return State::DECOMP_STATE_DUMMY;
        }

        return m_state;
    }

    auto handleDist2(std::uint8_t data) -> State
    {
        m_distanceDelta |= (std::int64_t)data;
        return State::DECOMP_STATE_GRAY;
    }
    auto handleDist3(std::uint8_t data) -> State
    {
        m_distanceDelta |= (std::int64_t)(static_cast<std::uint64_t>(data) << 8u);
        return State::DECOMP_STATE_DIST2;
    }
    auto handleDist4(std::uint8_t data) -> State
    {
        m_distanceDelta |= (std::int64_t)(static_cast<std::uint64_t>(data) << 16u);
        return State::DECOMP_STATE_DIST3;
    }
    auto handleGrey(std::uint8_t data) -> State
    {
        if ((m_sync = ((data & 0x40u) != 0x00u)))
        {
            m_distance = 0ul;
            m_grayValue = 0u;
            m_counter = 0ul;
        }
        m_filter = ((data & 0x20u) != 0x00u);

        if ((data & 0x80u) == 0x00u)
        {
            m_grayValueDelta = ((data & 0x10u) != 0u ? -0x10 + std::int64_t{data & 0x0Fu} : std::int64_t{data & 0x0Fu}) << 0u;
            m_distance += m_distanceDelta;
            m_grayValue += m_grayValueDelta;
            m_index = m_counter++;
            return State::DECOMP_STATE_DIST;
        }
        else
        {
            m_grayValueDelta = ((data & 0x10u) != 0u ? -0x10 + std::int64_t{data & 0x0Fu} : std::int64_t{data & 0x0Fu}) << 8u;
            return State::DECOMP_STATE_GRAY2;
        }

        return State::DECOMP_STATE_DIST;
    }
    auto handleGrey2(std::uint8_t data) -> State
    {
        m_grayValueDelta |= (std::int32_t)data;
        m_distance += m_distanceDelta;
        m_grayValue += m_grayValueDelta;
        m_index = m_counter++;
        return State::DECOMP_STATE_DIST;
    }

public:
    ScanDataDecompressor() = default;

    void reset()
    {
        m_state = State::DECOMP_STATE_DIST;
        m_counter = 0ul;
        m_index = 0ul;
    }

    bool isReady() const
    {
        return m_state == State::DECOMP_STATE_DIST;
    }

    auto getIndex() const -> std::size_t
    {
        return m_index;
    }

    auto getDistance() const -> std::uint64_t
    {
        return m_distance;
    }

    auto getGrayValue() const -> std::uint16_t
    {
        return m_grayValue;
    }

    auto getFilter() const -> bool
    {
        return m_filter;
    }

    bool process(std::uint8_t data)
    {
        auto action = [this] {
            switch (m_state)
            {
            default: // [[fallthrough]]
            case State::DECOMP_STATE_DUMMY:
                return &ScanDataDecompressor::handleDummy;
            case State::DECOMP_STATE_DIST:
                return &ScanDataDecompressor::handleDist;
            case State::DECOMP_STATE_DIST2:
                return &ScanDataDecompressor::handleDist2;
            case State::DECOMP_STATE_DIST3:
                return &ScanDataDecompressor::handleDist3;
            case State::DECOMP_STATE_DIST4:
                return &ScanDataDecompressor::handleDist4;
            case State::DECOMP_STATE_GRAY:
                return &ScanDataDecompressor::handleGrey;
            case State::DECOMP_STATE_GRAY2:
                return &ScanDataDecompressor::handleGrey2;
            }
        }();

        m_state = (this->*action)(data);
        return isReady();
    }
};
