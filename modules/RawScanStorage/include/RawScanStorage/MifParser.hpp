#pragma once
#include <cstdint>

class MIFParser
{
private:
    unsigned int wordsLeft{0u}; ///< defines how many dwords are still to read in current state (START_SYNC,FINISH_SYNC)
public:
    enum
    {
        SET_CNT,
        CHECK_CNT,             ///< this are states to verify counter (they are not used while regular scans)
        PS_SENSOR_DATA_0,      ///< sensor data package
        PS_SENSOR_DATA_1,      ///< sensor data package
        PS_SENSOR_DATA_LONG_0, ///< sensor data package
        PS_SENSOR_DATA_LONG_1, ///< sensor data package

        VENC_START, ///< v encoder package
        VENC_END,   ///< v encoder package
        HENC_START, ///< h encoder package
        HENC_END,   ///< h encoder package
        GENERAL_INFO_START,
        GENERAL_INFO_END,
        PS_SENSOR_DATA_LONG_REF_0, ///< states for reference measurements
        PS_SENSOR_DATA_LONG_REF_1,
        PS_SENSOR_DATA_REF_0,
        PS_SENSOR_DATA_REF_1,
        VERSION_INFO_0, ///< version/revision info package
        VERSION_INFO_1, ///< version/revision info package

        START_SYNC,  ///< waits for syncing sequence
        FINISH_SYNC, ///< waits for end of syncing sequence
        READ_HEADER  ///< just reads next word as header
    } state{READ_HEADER};

    std::uint32_t HEnc_value{0u}; ///< H encoder value
    std::uint32_t HEnc_value_prev{0u};

    MIFParser()
    {
    }

private:
    inline void procHeader(const std::int32_t &word)
    {
        /// detection of headers, (frequent packages first)
        if ((word & 0xE0000000) == 0x80000000)
        {
            state = PS_SENSOR_DATA_0;
        }
        else if ((word & 0xE0000000) == 0xA0000000)
        {
            state = PS_SENSOR_DATA_LONG_0;
        }
        else if ((word & 0xFC000000) == 0xC0000000)
        {
            state = VENC_START;
        }
        else if ((word & 0xFC000000) == 0xC4000000)
        {
            state = HENC_START;
        }
        else if ((word & 0xFC000000) == 0xC8000000)
        {
            state = GENERAL_INFO_START;
        }
        else if ((word & 0xFC000000) == 0xEC000000)
        {
            state = PS_SENSOR_DATA_LONG_REF_0;
        }
        else if ((word & 0xFC000000) == 0xCC000000)
        {
            state = PS_SENSOR_DATA_REF_0;
        }
        else if ((word & 0xFC000000) == 0xF8000000)
        {
            state = VERSION_INFO_0;
        }
        else if ((word == 0xFFFFFFFF))
        {
            state = FINISH_SYNC;
            wordsLeft = 1;
        }
        else
        {
            /// unknown header word -> we are out of sync, read from header
            state = READ_HEADER;
        }
    }

public:
    /// process given word
    inline void processWord(const std::uint32_t &word)
    {
        switch (state)
        {
        case PS_SENSOR_DATA_0:
            state = PS_SENSOR_DATA_1;
            break;
        case PS_SENSOR_DATA_LONG_0:
            state = PS_SENSOR_DATA_LONG_1;
            break;
        case VENC_START:
            state = VENC_END;
            break;
        case HENC_START:
            state = HENC_END;
            break;
        case GENERAL_INFO_START:
            state = GENERAL_INFO_END;
            break;
        case PS_SENSOR_DATA_LONG_REF_0:
            state = PS_SENSOR_DATA_LONG_REF_1;
            break;
        case PS_SENSOR_DATA_REF_0:
            state = PS_SENSOR_DATA_REF_1;
            break;
        case VERSION_INFO_0:
            state = VERSION_INFO_1;
            break;

        case PS_SENSOR_DATA_1:
        case PS_SENSOR_DATA_LONG_1:
        case VENC_END:
        case HENC_END:
        case GENERAL_INFO_END:
        case PS_SENSOR_DATA_REF_1:
        case PS_SENSOR_DATA_LONG_REF_1:
        case VERSION_INFO_1:
        case READ_HEADER:
            procHeader(word);
            break;

        case START_SYNC:
            if ((word) == 0xFFFFFFFFu)
            {

                state = FINISH_SYNC;
                wordsLeft = 1;
            }
            //   OutputDebugString(L"---START_SYNC---\n");
            break;
        case FINISH_SYNC:
            if ((word) == 0xFFFFFFFF)
            {
                if (wordsLeft > 0)
                    wordsLeft--;
            }
            else
            {
                if (wordsLeft == 0)
                {

                    procHeader(word);
                }
                else
                    state = START_SYNC;
            }

            break;
        default:
            break;
        }

        switch (state)
        {
        case HENC_END:
            HEnc_value_prev = HEnc_value;
            HEnc_value = word;
            break;
        default:
            break;
        }
    }
};