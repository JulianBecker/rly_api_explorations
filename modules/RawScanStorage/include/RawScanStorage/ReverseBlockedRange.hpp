#pragma once
#include <utility>
#include <cstdint>

class ReverseBlockIterator
{
    std::size_t m_blockSize;
    std::uint8_t const *m_data;
    std::size_t m_dataSize;
    std::size_t m_index;
    std::uint8_t m_value;

    void moveNext()
    {
        m_index++;
        m_value = m_data[(m_index & 0xFFF0u) + 0x0Fu - (m_index & 0x000fu)];
    }

public:
    ReverseBlockIterator()
        : m_blockSize{0ul}, m_data{nullptr}, m_dataSize{0ul}, m_index{0ul}, m_value{0u}
    {}

    ReverseBlockIterator(std::size_t blockSize, std::uint8_t const *data, std::size_t dataSize, std::size_t index)
        : m_blockSize{std::move(blockSize)}, m_data{data}, m_dataSize{dataSize}, m_index{index}, m_value{}
    {
        if (index < dataSize)
            m_value = data[(index & 0xFFF0u) + 0x0Fu - (index & 0x000fu)];
    }

    friend bool operator==(const ReverseBlockIterator &a, const ReverseBlockIterator &b)
    {
        return a.m_index == b.m_index;
    }

    friend bool operator!=(const ReverseBlockIterator &a, const ReverseBlockIterator &b)
    {
        return !(a == b);
    }

    auto operator*() const -> std::uint8_t
    {
        return m_value;
    }

    auto operator++() -> ReverseBlockIterator &
    {
        moveNext();
        return *this;
    }

    auto operator++(int) -> ReverseBlockIterator
    {
        auto copy(*this);
        ++(*this);
        return copy;
    }
};

class ReverseBlockedRange
{
    std::size_t m_blockSize;
    std::uint8_t const *m_data;
    std::size_t m_dataSize;

public:
    ReverseBlockedRange(std::size_t blockSize, std::uint8_t const *data, std::size_t dataSize)
        : m_blockSize{std::move(blockSize)}, m_data{std::move(data)}, m_dataSize{std::move(dataSize)}
    {
    }

    auto begin() const -> ReverseBlockIterator
    {
        return {m_blockSize, m_data, m_dataSize, 0ul};
    }

    auto end() const -> ReverseBlockIterator
    {
        return {
            m_blockSize,
            m_data,
            m_dataSize,
            m_dataSize};
    }
};
