#pragma once

struct ScannerCompensationParams
{
	double distFactor;
	double numVerEncoderSteps; // VertEncoderSteps
	double numPointsPerRevolution;
	double numHorEncoderSteps;

	double triggerOffset;		 // F_MIRROR::A_TRIGGEROFFSET
	double mirrorAdjustment;	 // F_MIRROR::A_MIRRORADJUSTMENT
	double mirrorAxisAdjustment; // F_BASIS::A_MIRRORAXISADJUSTMENT
	double vertLaser;			 // F_SENSOR::A_VERTLASERADJUSTMENT
	double horLaser;			 // F_SENSOR::A_HORLASERADJUSTMENT
	double housingOffsetX;		 // F_BASIS::A_HOUSING_OFFSET[0]
	double housingOffsetY;		 // F_BASIS::A_HOUSING_OFFSET[1]
	double laserOffsetX;		 // F_SENSOR::A_LASEROFFSET[0]
	double laserOffsetY;		 // F_SENSOR::A_LASEROFFSET[1]
	double laserOffsetZ;		 // F_SENSOR::A_LASEROFFSET[2]
};