#pragma once
#include "ScannerCompensationParams.hpp"

#include <Eigen/Eigen>
#include <cmath>
#if __has_include(<numbers>)
#    include <numbers>
#else
namespace std::numbers {
constexpr double pi = M_PI;
}

#endif

static Eigen::Vector4d applyAnglesCompensation(const ScannerCompensationParams& params, const double horEnc,
                                               const double vertIndex, const double distanceRaw) {
    constexpr double twoPI = 2.0 * std::numbers::pi;

    const double vertAngle = vertIndex / params.numPointsPerRevolution * twoPI;
    const double theta = -std::numbers::pi / 2.0 + vertAngle;
    const double phi = horEnc / params.numHorEncoderSteps * twoPI;

    const double& faro_theta = theta;
    const double& camZRot = params.horLaser;
    const double& camZRotCos = std::cos(params.horLaser);
    const double& camZRotSin = std::sin(params.horLaser);

    const double& tau = params.triggerOffset;

    const double& alpha = params.mirrorAxisAdjustment;
    const double& alphaSin = std::sin(params.mirrorAxisAdjustment);
    const double& alphaCos = std::cos(params.mirrorAxisAdjustment);

    const double& camXRot = params.vertLaser;

    constexpr double pi = std::numbers::pi;
    const double& lx = params.laserOffsetX;
    const double& ly = params.laserOffsetY;
    const double& lz = params.laserOffsetZ;
    const double& px = params.housingOffsetX;
    const double& py = params.housingOffsetY;

    double t1 = std::sin(params.vertLaser);
    double t3 = camZRotSin;
    double t4 = 1.0;
    double t5 = camZRotCos;
    double t7 = t5;
    double t8 = t7;
    double t9 = std::sin(-faro_theta + tau);
    double t10 = std::cos(-faro_theta + tau);

    double t12 = std::cos(params.mirrorAdjustment + 0.3e1 / 0.4e1 * pi);

    double t15 = -t3;

    double t16 = std::cos(params.mirrorAdjustment + 0.5e1 / 0.4e1 * pi);

    double t17 = std::cos(params.vertLaser);
    double t18 = t8 * t10;
    double t19 = -t18 * t12 + t15 * t16;

    double t20 = std::sin(2. * params.mirrorAdjustment) + 1.;

    double t21 = 1. / t12;
    t15 = t18 * t16 + t15 * t20 * t21 / 0.2e1;
    t18 = -t19 * t16 + t15 * t12;

    double t23 = alphaCos;
    double t2 = t8 * t9;

    t8 = std::sin(params.mirrorAdjustment + 0.3e1 / 0.4e1 * pi);

    double t11 = t19 * t12 - t15 * t8;
    t15 = t11 * t10 + t2 * t9;

    double t28 = std::cos(phi);
    double t22 = std::sin(phi);

    t2 = -t11 * t9 + t2 * t10;
    t11 = alphaSin;
    double t24 = t18 * t23 + t2 * t11;
    double t13 = t3 * t1;
    double t6 = t7 * t1;
    t7 = t9 * t17;
    double t14 = t13 * t10 + t7 * t4;
    double t25 = -t14 * t12 + t6 * t16;
    t6 = t14 * t16 + t6 * t20 * t21 / 0.2e1;
    t14 = -t25 * t16 + t6 * t12;
    t4 = t13 * t9 - t17 * t4 * t10;
    t6 = t25 * t12 - t6 * t8;
    t13 = t6 * t10 + t4 * t9;
    t4 = -t6 * t9 + t4 * t10;
    t6 = t14 * t23 + t4 * t11;
    t25 = t3 * t10;
    double t26 = t1 * t9;
    double t27 = (-t25 * t12 + t5 * t16) * t17 + t26 * t12;
    t5 = (t20 * t5 * t21 / 2. + t25 * t16) * t17 - t26 * t16;
    t16 = -t27 * t16 + t5 * t12;
    t1 = t1 * t10 + t7 * t3;
    t3 = t27 * t12 - t5 * t8;
    t5 = t3 * t10 + t1 * t9;
    t1 = -t3 * t9 + t1 * t10;
    t3 = t16 * t23 + t1 * t11;

    Eigen::Matrix4d cg;
    cg(0, 0) = (t24 * t22 + t15 * t28);
    cg(1, 0) = (t24 * t28 - t15 * t22);
    cg(2, 0) = (-t18 * t11 + t2 * t23);
    cg(3, 0) = (t24 * py + lx + px * t15);
    cg(0, 1) = (t6 * t22 + t13 * t28);
    cg(1, 1) = (t6 * t28 - t13 * t22);
    cg(2, 1) = (-t14 * t11 + t4 * t23);
    cg(3, 1) = (t6 * py - lz + px * t13);
    cg(0, 2) = (t3 * t22 + t5 * t28);
    cg(1, 2) = (t3 * t28 - t5 * t22);
    cg(2, 2) = (-t16 * t11 + t1 * t23);
    cg(3, 2) = (t3 * py + ly + px * t5);
    cg(0, 3) = 0;
    cg(1, 3) = 0;
    cg(2, 3) = 0;
    cg(3, 3) = 1;

    return cg * Eigen::Vector4d(0.0, 0.0, distanceRaw * params.distFactor + params.laserOffsetY, 1.0);
}
