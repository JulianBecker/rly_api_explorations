#pragma once
#include <FileIO/MemoryMappedFile.hpp>

#include <array>
#include <vector>

struct FblPacketRef {
    std::size_t payloadSize;
    std::array<char, 3ul> cmdId;
    std::byte const* payload;
};

class ScanDataStreamIterator {
    std::shared_ptr<std::vector<MemoryMappedFile>> m_loaders;
    MemoryMappedFile* m_activeLoader{nullptr};
    std::size_t m_fileIndex;
    std::byte const* m_p{nullptr};
    FblPacketRef m_packet;

    bool loadNext() {
        if (m_fileIndex + 1u < m_loaders->size()) {
            // m_activeLoader->unmap();
            m_activeLoader = &(*m_loaders)[++m_fileIndex];
            // m_activeLoader->map();
            m_p = m_activeLoader->data();
            return true;
        }
        return false;
    }

    void moveNext() {
        constexpr std::size_t fblHeaderLength = 9ul;
        constexpr auto headerSignatureMatch = [](std::byte const* p) {
            return p[0] == static_cast<std::byte>('!') && p[2] == static_cast<std::byte>('$') &&
                   p[6] == static_cast<std::byte>('$');
        };
        while (m_p) {
            std::byte const* p{m_p};
            for (std::size_t remaining = (m_activeLoader->data() + m_activeLoader->size()) - p;
                 remaining >= fblHeaderLength; ++p, remaining--) {
                if (headerSignatureMatch(p)) {
                    const std::uint16_t payloadSize =
                        static_cast<std::uint16_t>(p[7]) | (static_cast<std::uint16_t>(p[8]) << 8u);
                    m_packet = FblPacketRef{payloadSize, std::array<char, 3>{(char) p[3], (char) p[4], (char) p[5]},
                                            p + fblHeaderLength};
                    m_p = p + fblHeaderLength + payloadSize;
                    return;
                }
            }
            if (!loadNext())
                break;
        }
        m_fileIndex = m_loaders->size();
        m_p = nullptr;
    };

public:
    ScanDataStreamIterator(std::shared_ptr<std::vector<MemoryMappedFile>> loaders, std::size_t fileIndex)
        : m_loaders{std::move(loaders)}
        , m_fileIndex{fileIndex} {
        if (fileIndex < m_loaders->size()) {
            m_activeLoader = &m_loaders->operator[](fileIndex);
            // m_activeLoader->load();
            m_p = m_activeLoader->data();
            moveNext();
        }
    }

    friend bool operator==(const ScanDataStreamIterator& a, const ScanDataStreamIterator& b) {
        return a.m_fileIndex == b.m_fileIndex && a.m_p == b.m_p;
    }

    friend bool operator!=(const ScanDataStreamIterator& a, const ScanDataStreamIterator& b) {
        return !(a == b);
    }

    auto operator*() const -> FblPacketRef const& {
        return m_packet;
    }

    auto operator->() const -> FblPacketRef const* {
        return &m_packet;
    }

    auto operator++() -> ScanDataStreamIterator& {
        moveNext();
        return *this;
    }

    auto operator++(int) -> ScanDataStreamIterator {
        auto copy(*this);
        ++(*this);
        return copy;
    }
};