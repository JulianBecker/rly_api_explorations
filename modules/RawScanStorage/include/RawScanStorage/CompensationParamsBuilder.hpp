#pragma once
#include "ScannerCompensationParams.hpp"

#include <AttributeContainer/AttrContainer.hpp>

#include <map>

static auto convertTriggerModeToSubsample(int triggerMode) -> int {
    /// TriggerMode: Defines the method used to trigger measurements
    /// by the vertical angle encoder
    enum TriggerMode {
        NoTrigger = 0, ///< Don't trigger
        Encoder = 1,   ///< Trigger by the number of encoder signals
        Encoder_2 = 2, ///< Trigger by the number of encoder signals / 2
        // 3 is missing because of compatibility reasons !
        Encoder_4 = 3,   ///< Trigger by the number of encoder signals / 4
        Encoder_5 = 4,   ///< Trigger by the number of encoder signals / 5
        Encoder_8 = 7,   ///< Trigger by the number of encoder signals / 8
        Encoder_10 = 9,  ///< Trigger by the number of encoder signals / 10
        Encoder_16 = 16, ///< Trigger by the number of encoder signals / 16
        Encoder_20 = 20, ///< Trigger by the number of encoder signals / 20
        Encoder_32 = 32, ///< Trigger by the number of encoder signals / 32
        Encoder_40 = 40, ///< Trigger by the number of encoder signals / 40
        Encoder_64 = 64, ///< Trigger by the number of encoder signals / 64
    };

    return std::map<int, int>{{NoTrigger, 1},   {Encoder, 1},     {Encoder_2, 2},   {Encoder_4, 4},
                              {Encoder_5, 5},   {Encoder_8, 8},   {Encoder_10, 10}, {Encoder_16, 16},
                              {Encoder_20, 20}, {Encoder_32, 32}, {Encoder_40, 40}, {Encoder_64, 64}}
        .at(triggerMode);
}

static auto extractCompensationParams(const attrs::AttrContainer& scannerParams) -> ScannerCompensationParams {
    // const auto usedMirrorVelocity = scannerParams["MirrorField"].get<attrs::Double>("UsedMirrorVelocity").data;
    // const auto vertResolutionFactor = scannerParams["MirrorField"].get<attrs::Int>("VertResolutionFactor").data;
    const auto triggerMode = scannerParams["SensorField"].get<attrs::Int>("TriggerMode").data;
    const auto subSampling = convertTriggerModeToSubsample(triggerMode);
    // const auto measurementRate = scannerParams["SensorField"].get<attrs::Double>("MeasurementRate").data;
    const auto verEncoderSteps = scannerParams["MirrorField"].get<attrs::Int>("VertEncoderSteps").data;
    const auto numPointsPerRevolution = std::round(verEncoderSteps / subSampling);

    auto params = ScannerCompensationParams{
        scannerParams["SensorField"].get<attrs::Double>("DistFactor").data * 1e-3,
        static_cast<double>(verEncoderSteps),
        numPointsPerRevolution,
        static_cast<double>(scannerParams["BasisField"].get<attrs::Int>("HorEncoderSteps").data),
        scannerParams["MirrorField"].get<attrs::Double>("TriggerOffset").data,
        scannerParams["MirrorField"].get<attrs::Double>("MirrorAdjustment").data,
        scannerParams["BasisField"].get<attrs::Double>("MirrorAxisAdjustment").data,
        scannerParams["SensorField"].get<attrs::Double>("VertLaserAdjustment").data,
        scannerParams["SensorField"].get<attrs::Double>("HorLaserAdjustment").data,
        scannerParams["BasisField"].get<attrs::SVec2dd>("HousingOffset").data[0],
        scannerParams["BasisField"].get<attrs::SVec2dd>("HousingOffset").data[1],
        scannerParams["SensorField"].get<attrs::Vec3d>("LaserOffset").data[0],
        scannerParams["SensorField"].get<attrs::Vec3d>("LaserOffset").data[1],
        scannerParams["SensorField"].get<attrs::Vec3d>("LaserOffset").data[2]};
    return params;
}