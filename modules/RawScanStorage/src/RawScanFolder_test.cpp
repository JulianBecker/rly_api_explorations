#include <RawScanStorage/RawScanFolder.hpp>
#include <catch2/catch.hpp>

#include <fstream>
#include <random>
#include <sstream>


TEST_CASE("Extracting stream index from ScanDataStream filename", "[streamIndexFromFileName]") {
    REQUIRE(streamIndexFromFileName("ScanDataStream0") == 0);
    REQUIRE(streamIndexFromFileName("ScanDataStream2") == 2);
    REQUIRE(streamIndexFromFileName("ScanDataStream3") == 3);
    REQUIRE(streamIndexFromFileName("ScanDataStream10") == 10);
    REQUIRE(streamIndexFromFileName("ScanDataStream4") == 4);
    REQUIRE(streamIndexFromFileName("ScanDataStream1") == 1);
    REQUIRE(streamIndexFromFileName("ScanDataStream11") == 11);
    REQUIRE(streamIndexFromFileName("ScanDataStream13") == 13);
    REQUIRE(streamIndexFromFileName("ScanDataStream100") == 100);
    REQUIRE(streamIndexFromFileName("ScanDataStream103") == 103);
}

TEST_CASE("Sorting of ScanDataStreams", "[sortScanDataStreams]") {
    GIVEN("A collection of ScanDataStreams") {
        std::vector<std::filesystem::path> paths{
            std::filesystem::path{"Scans"} / "1" / "ScanDataStream0",
            std::filesystem::path{"Scans"} / "1" / "ScanDataStream2",
            std::filesystem::path{"Scans"} / "1" / "ScanDataStream3",
            std::filesystem::path{"Scans"} / "1" / "ScanDataStream10",
            std::filesystem::path{"Scans"} / "1" / "ScanDataStream4",
            std::filesystem::path{"Scans"} / "1" / "ScanDataStream1",
            std::filesystem::path{"Scans"} / "1" / "ScanDataStream11",
            std::filesystem::path{"Scans"} / "1" / "ScanDataStream13",
            std::filesystem::path{"Scans"} / "1" / "ScanDataStream100",
            std::filesystem::path{"Scans"} / "1" / "ScanDataStream103",
        };
        WHEN("we sort them") {
            sortScanDataStreams(paths.begin(), paths.end());
            THEN("the resulting paths are ordered") {
                REQUIRE(paths.at(0) == std::filesystem::path{"Scans"} / "1" / "ScanDataStream0");
                REQUIRE(paths.at(1) == std::filesystem::path{"Scans"} / "1" / "ScanDataStream1");
                REQUIRE(paths.at(2) == std::filesystem::path{"Scans"} / "1" / "ScanDataStream2");
                REQUIRE(paths.at(3) == std::filesystem::path{"Scans"} / "1" / "ScanDataStream3");
                REQUIRE(paths.at(4) == std::filesystem::path{"Scans"} / "1" / "ScanDataStream4");
                REQUIRE(paths.at(5) == std::filesystem::path{"Scans"} / "1" / "ScanDataStream10");
                REQUIRE(paths.at(6) == std::filesystem::path{"Scans"} / "1" / "ScanDataStream11");
                REQUIRE(paths.at(7) == std::filesystem::path{"Scans"} / "1" / "ScanDataStream13");
                REQUIRE(paths.at(8) == std::filesystem::path{"Scans"} / "1" / "ScanDataStream100");
                REQUIRE(paths.at(9) == std::filesystem::path{"Scans"} / "1" / "ScanDataStream103");
            }
        }
    }
}

// https://stackoverflow.com/a/58454949/2622629
static auto create_temporary_directory(unsigned long long max_tries = 1000) -> std::filesystem::path {
    auto tmp_dir = std::filesystem::temp_directory_path();
    unsigned long long i = 0;
    std::random_device dev;
    std::mt19937 prng(dev());
    std::uniform_int_distribution<uint64_t> rand(0);
    std::filesystem::path path;
    while (true) {
        std::stringstream ss;
        ss << std::hex << rand(prng);
        path = tmp_dir / ss.str();
        // true if the directory was created.
        if (std::filesystem::create_directory(path)) {
            break;
        }
        if (i == max_tries) {
            throw std::runtime_error("could not find non-existing directory");
        }
        i++;
    }
    return path;
}

TEST_CASE("Listing ScanDataStream files in .fls RawScan directory", "[findScanDataStreams]") {
    GIVEN("A raw scan folder containing some ScanDataStreams") {
        const auto tempDir{create_temporary_directory()};
        std::filesystem::create_directories(tempDir / "Scans" / "1");
        std::ofstream(tempDir / "Scans" / "1" / "ScanDataStream0");
        std::ofstream(tempDir / "Scans" / "1" / "ScanDataStream8");
        std::ofstream(tempDir / "Scans" / "1" / "ScanDataStream9");
        std::ofstream(tempDir / "Scans" / "1" / "ScanDataStream10");
        std::ofstream(tempDir / "Scans" / "1" / "ScanDataStream11");
        std::ofstream(tempDir / "Scans" / "1" / "ScanDataStream100");
        WHEN("the list of data streams is requested") {
            auto dataStreamPaths = findScanDataStreams(tempDir);
            THEN("all available streams are found and listed in the right order") {
                REQUIRE(dataStreamPaths.size() == 6);
                REQUIRE(dataStreamPaths.at(0).filename().string() == "ScanDataStream0");
                REQUIRE(dataStreamPaths.at(1).filename().string() == "ScanDataStream8");
                REQUIRE(dataStreamPaths.at(2).filename().string() == "ScanDataStream9");
                REQUIRE(dataStreamPaths.at(3).filename().string() == "ScanDataStream10");
                REQUIRE(dataStreamPaths.at(4).filename().string() == "ScanDataStream11");
                REQUIRE(dataStreamPaths.at(5).filename().string() == "ScanDataStream100");
            }
        }
    }
}

TEST_CASE("Playground", "[ScanDataReader]") {
    std::ofstream outfile("data1.xyz");

    auto start = std::chrono::steady_clock::now();
    auto scan = ScanFolder(std::filesystem::path{"assets/JB_2MHZ_Validation_039_r32_q4.fls"});

    std::size_t numPts{0ul};
    for (const auto& [pt, gray] : scan.points()) {
        if (gray < 500)
            continue;
        // std::cout << "dist: " << (decomp.getDistance() * distFactor) << ", refl: " << decomp.getGrayValue() << "\n";
        outfile << pt.x() << "\t" << pt.y() << "\t" << pt.z() << "\t" << gray << "\n";
        ++numPts;
    }

    auto duration =
        std::chrono::duration_cast<std::chrono::duration<double>>(std::chrono::steady_clock::now() - start).count();

    std::cout << "duration: " << duration << "s\n"
              << "found " << numPts << " points\n";
}
