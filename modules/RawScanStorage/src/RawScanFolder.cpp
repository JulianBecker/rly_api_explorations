#include <RawScanStorage/RawScanFolder.hpp>

auto streamIndexFromFileName(std::string const& fileName) -> std::optional<std::size_t> {
    static std::regex scanDataStreamRegex("ScanDataStream([[:digit:]]+)");
    std::smatch match;
    if (!std::regex_match(fileName, match, scanDataStreamRegex) || match.size() != 2u)
        return std::nullopt;

    int streamIndex{};
    const auto matchStr = match[1].str();
    std::from_chars(matchStr.c_str(), matchStr.c_str() + matchStr.size(), streamIndex);
    return {streamIndex};
}

auto findScanDataStreams(const std::filesystem::path& rawScanFolderPath) -> std::vector<std::filesystem::path> {
    const std::regex scanDataStreamRegex("ScanDataStream([[:digit:]]+)");
    std::smatch match;
    std::vector<std::filesystem::path> scanDataStreamPaths;
    for (const auto& p : std::filesystem::recursive_directory_iterator(
             rawScanFolderPath, std::filesystem::directory_options::skip_permission_denied)) {
        if (!p.is_regular_file())
            continue;

        if (!streamIndexFromFileName(p.path().filename().string()))
            continue;

        scanDataStreamPaths.push_back(p.path());
    }
    sortScanDataStreams(scanDataStreamPaths.begin(), scanDataStreamPaths.end());
    return scanDataStreamPaths;
}